import Guide_1_true from 'public/icon-guide/guide-1-1.svg';
import Guide_1_false from 'public/icon-guide/guide-1-2.svg';
import Guide_2_true from 'public/icon-guide/guide-2-1.svg';
import Guide_2_false from 'public/icon-guide/guide-2-2.svg';
import Guide_3_true from 'public/icon-guide/guide-3-1.svg';
import Guide_3_false from 'public/icon-guide/guide-3-2.svg';
import Guide_4_true from 'public/icon-guide/guide-4-1.svg';
import Guide_4_false from 'public/icon-guide/guide-4-2.svg';
import Guide_5_true from 'public/icon-guide/guide-5-1.svg';
import Guide_5_false from 'public/icon-guide/guide-5-2.svg';


export const Guide = {
    ContentGuide: [
        {
            textId: 'Pastikan foto tegak lurus dan sejajar dengan soal.',
            IconElementTrue: Guide_1_true,
            IconElementFalse: Guide_1_false
        },
        {
            textId: 'Pastikan foto berupa teks ketikan dan bukan tulisan tangan.',
            IconElementTrue: Guide_2_true,
            IconElementFalse: Guide_2_false
        },
        {
            textId: 'Crop (potong) foto sejumlah satu soal.',
            IconElementTrue: Guide_3_true,
            IconElementFalse: Guide_3_false
        },
        {
            textId: 'Posisi foto harus berdiri / portrait.',
            IconElementTrue: Guide_4_true,
            IconElementFalse: Guide_4_false
        },
        {
            textId: 'Pastikan foto jelas dan terang, tidak gelap ataupun blur.',
            IconElementTrue: Guide_5_true,
            IconElementFalse: Guide_5_false
        },
    ]
};
