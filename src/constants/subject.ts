export const Subject = {
  Matematika: {
    Id: 1,
    Text: 'subject.math'
  },
  Fisika: {
    Id: 2,
    Text: 'subject.physics'
  },
  Kimia: {
    Id: 3,
    Text: 'subject.chemistry'
  },
}

export const Major = {
  Saintek: {
    Id: 1,
    Text: 'subject.saintek'
  },
  Soshum: {
    Id: 2,
    Text: 'subject.soshum'
  },
  Campuran: {
    Id: 3,
    Text: 'subject.campuran'
  },
}