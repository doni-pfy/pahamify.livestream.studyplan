export const ResizeSettings = {
  MAX_WIDTH: 720,
  MAX_HEIGHT: 720,
  MIN_WIDTH: 200,
  MIN_HEIGHT: 200,
  OUTPUT_FORMAT: {
    JPEG: "JPEG",
    PNG: "PNG",
    WEBP: "WEBP"
  },
  OUTPUT_TYPE: {
    Base64: "base46",
    Blob: "blob",
    File: "file"
  }
}