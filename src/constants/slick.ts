export const Slick = {
  Settings: {
    dots: false,
    arrows: false,
    infinite: false,
    variableWidth: true,
    swipeToSlide: true,
  }
}