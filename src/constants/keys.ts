export const TOKEN_KEY = "vuex";
export const TOKEN_EXPIRY_DAY = 3;
export const PUBLIC_API_CLIENT = 'public';
export const AUTH_API_CLIENT = 'auth';
