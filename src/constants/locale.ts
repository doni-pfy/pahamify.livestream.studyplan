import { intlMessagesId } from 'src/locale/id';

export const LOCALE_ID = 'id';

export const MESSAGES = {
  [LOCALE_ID]: intlMessagesId
};
