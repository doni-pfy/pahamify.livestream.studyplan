export const SliderSettings = {
  Style: {
    track: {
      backgroundColor: '#EAEFF7',
      backgroundImage: 'url(data:image/svg+xml;base64,PHN2ZyB4bWxucz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciIHZlcnNpb249IjEuMSIgcHJlc2VydmVBc3BlY3RSYXRpbz0ibm9uZSIgd2lkdGg9IjQiIGhlaWdodD0iOCI+PGxpbmUgeDE9IjIiIHkxPSIwIiB4Mj0iMiIgeTI9IjgiIHN0cm9rZT0iYmxhY2siIHZlY3Rvci1lZmZlY3Q9Im5vbi1zY2FsaW5nLXN0cm9rZSIvPjwvc3ZnPg==)',
      backgroundRepeat: 'repeat-x',
      backgroundPosition: 'left center',
      backgroundSize: '5px',
      height: 15,
      width: '100%'
    },
    active: {
      display: 'none'
    },
    thumb: {
      width: 3,
      height: 15,
      opacity: 1,
      backgroundColor: 'black',
      borderRadius: 0
    }
  }
}