export const ProfileDefault = {
  PROFILE_PICTURE_URL: '/empty-profile.png',
  BIO: 'Belajar bareng Mipi, seru!',
};

export const ProfileValidation = {
  MAX_BIO_LENGTH: 50,
};