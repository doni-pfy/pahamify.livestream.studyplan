export const MapelColor = {
  Matematika: '#91E3FF',
  Kimia: '#FFCB64',
  Biologi: '#AEE073',
  Fisika: '#ED93B3',
  Sosiologi: '#CD96FF',
  Geografi: '#78EFB3',
  Ekonomi: '#FFF069',
  Sejarah: '#C7A27C',
  All: '#6C96D3'
}