import Keunggulan_1 from 'public/icon-keunggulan/Keunggulan-1.svg';
import Keunggulan_2 from 'public/icon-keunggulan/Keunggulan-2.svg';
import Keunggulan_3 from 'public/icon-keunggulan/Keunggulan-3.svg';

export const Feature = {
  Content: [
    {
      ImageFeature: Keunggulan_1,
      title: "Mudah dan Cepat",
      text: "Temukan pembahasan atau diskusi hanya dengan foto soal.",
    },
    {
      ImageFeature: Keunggulan_2,
      title: "Lengkap",
      text: "Pembahasan melalui video menarik yang mempermudah pemahaman.",
    },
    {
      ImageFeature: Keunggulan_3,
      title: "Forum Diskusi",
      text: "Bahas soalmu bersama pengguna lainnya, juga Rockstar Teacher!",
    },
  ]
}