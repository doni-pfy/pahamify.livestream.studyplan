export const Testimoni = {
  Content: [
    {
      imageUrl: "/testimoni/ariq.jpg",
      name: "Muhamad Ariq Naufal",
      kelas: "Kelas XII-Lampung",
      testi: "Tanya Mipi membantu untuk cari soal2 yg susah. Sehari kadang cari soal 1-2 kali",
    },
    {
      imageUrl: "/testimoni/febri.jpg",
      name: "Febri Haryadi",
      kelas: "Kelas XI-Riau",
      testi: "Fitur ini membantu. Saat review soal yang belum paham. Fitur ini sangat membantu proses belajar",
    },
    {
      imageUrl: "/testimoni/ilham.jpg",
      name: "Ilham Juliansyah",
      kelas: "Kelas XII-Bekasi",
      testi: "Terbantu banget! Apalagi kelas 12, banyak soal-soal yg susah",
    },
    {
      imageUrl: "/testimoni/saka.jpg",
      name: "Saka Gunasari",
      kelas: "Kelas XII-Kalsel",
      testi: "Dari dulu bingung cari aplikasi untuk mengerjakan soal matematika, sekarang tinggal Tanya Mipi Aja!",
    },
    {
      imageUrl: "/testimoni/zelaya.jpg",
      name: "Zelaya Winadarma",
      kelas: "Kelas XI-Jakarta",
      testi: "Aku sih terbantu banget sama Tanya Mipi, soalnya jarang ada aplikasi mengerjakan soal Fisika.",
    },
    {
      imageUrl: "/testimoni/usman.jpg",
      name: "Usman Manujiwa",
      kelas: "Kelas XI-Bogor",
      testi: "Semoga aplikasi untuk menjawab soal fisika seperti Tanya Mipi terus membantu kita belajar.",
    },
    {
      imageUrl: "/testimoni/kukuh.jpg",
      name: "Kukuh Binama",
      kelas: "Kelas XII-Bali",
      testi: "Waktu pandemi kayak gini, susah diskusinya. Aplikasi matematika android ngebantu banget pas ada soal susah.",
    },
  ]
}