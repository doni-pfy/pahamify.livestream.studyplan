export const MipiChances = 'mipiChances';
export const MipiSubjectId = 'mipiSubjectId';
export const Image = 'image';
export const ProfileBio = 'profileBio';
export const ProfilePicture = 'profilePicture';