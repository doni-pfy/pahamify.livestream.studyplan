export const Camera = {
  FRONT: 'user',
  BACK: 'environment'
}

export const Canvas = "canvas";
export const Context = {
  "2D": "2d",
  "WEBGL": "Webgl"
}