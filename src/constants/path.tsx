export const Path = {
  Forum: '/forum',
  Account: '/account',
  Profile: '/profile',
  TakeProfilePicture: '/profile/take-picture',
  EditProfilePicture: '/profile/edit-picture',
  EditProfileBio: '/profile/edit-bio',
  EditImage: '/edit-image',
  Root: '/',
  ContentPath: [
    {
      url: "/guide",
      titleId: "page.title.guide",
    },
    {
      url: "/tanya-mipi",
      titleId: "page.title.ask.mipi",
    },
    {
      url: "/edit-image",
      titleId: "page.title.edit.photo",
    },
    {
      url: "/login",
      titleId: "page.title.login",
    },
    {
      url: "/video",
      titleId: "empty.string",
    },
    {
      url: "/faq",
      titleId: "page.title.faq",
    },
    {
      url: "/service-terms",
      titleId: "page.title.service.terms",
    },
    {
      url: "/privacy-policy",
      titleId: "page.title.privacy.policy",
    },
    {
      url: "/profile/take-picture",
      titleId: "page.title.profile.picture.take",
    },
    {
      url: "/profile/edit-picture",
      titleId: "page.title.edit.photo",
    },
    {
      url: "/profile/edit-bio",
      titleId: "page.title.profile.bio.edit",
    },
    {
      url: "/account",
      titleId: "page.title.account",
    },
  ],
  TransparentHeaderPath: [
    '/login',
    '/profile',
    '/result/solution/[searchId]'
  ],
  ExcludeFooterPath: [
    '/',
    '/tanya-mipi',
    '/login',
    "/edit-image",
    '/profile',
    "/profile/edit-picture",
    "/profile/edit-bio",
    "/studyplan",
    "/studyplan/details",
  ],
  ExcludeBottomNavPath: [
    '/',
    '/tanya-mipi',
    '/login',
    "/edit-image",
    "/profile/edit-picture",
    "/profile/edit-bio",
    "/studyplan",
    "/studyplan/details",
  ]
};
