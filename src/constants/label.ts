export const LabelConfidence = {
  SangatMirip: {
    threshold: 0.8,
    text: "label.mirip"
  },
  CukupMirip: {
    threshold: 0.7,
    text: "label.cukup.mirip"
  },
  MungkinMirip: {
    threshold: 0.5,
    text: "label.mungkin.mirip"
  },
}