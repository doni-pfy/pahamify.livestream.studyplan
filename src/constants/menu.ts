export const TAB_MENU = {
  FOTO: "foto",
  FORUM: "forum",
  HISTORY: "history",
  PROFILE: "profile",
  MIPI_CAMERA: "camera",
  MIPI_TYPE_QUESTION: "type_question",
  PHOTO_ROTATE: "rotate",
  PHOTO_SCALE: "scale"
};
