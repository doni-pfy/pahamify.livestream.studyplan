export interface DataFilterType {
  title: string;
  label: string;
  data: FilterType[];
  filtered: Array<number>
}

export interface FilterType {
  id: number;
  text: String;
}

export interface FilterResultType {
  bab: Array<number>;
  kelas: Array<number>;
}