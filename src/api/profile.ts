import { gql } from "@apollo/client";

export const GET_USER_SEARCH = gql`
    query GetUserSearchResults($Page: Int!, $PageSize: Int!) {
        GetUserSearchResults(Page:$Page, PageSize:$PageSize) {
            Data {
                UserID
                CreatedAt
                ImagePath
                CourseGroupID
                BestConfidence
                Content
                SearchResultStatus
                ID
            }
        }
    }
`

export const GET_FORUM_REPORT_PROFILE = gql`
  query {
    GetForumProfile {
      Bio
      ProfilePictureURL
      Point
    }
  }
`;

export const GET_PROFILE = gql`
   query GetProfile {
    profile
      @rest(type: "Profile", path: "/auth/profile") {
        FullName
        Class
        School
    }
  }
`;

export const UPDATE_PROFILE = gql`
  mutation UpdateProfile($profilePictureUrl: String, $bio: String) {
    UpdateForumProfile(
      Bio: $bio,
      ProfilePictureURL: $profilePictureUrl
    )
  }
`;

export const UPLOAD_PROFILE_PICTURE = gql`
  mutation UploadImage($input: File!, $basePath: String!) {
    uploadImage(input: $input, basePath: $basePath) 
      @rest(type: "File", path: "/upload/s3?path={args.basePath}/profile/", method: "POST", bodySerializer: "fileEncode") {
        path
      }
  }
`;