import { gql } from "@apollo/client";

export const SEARCH_BY_TEXT = gql`
  mutation SearchByText($Content: String!, $CourseGroupID: Int!) {
    SearchByText(Content: $Content, CourseGroupID: $CourseGroupID) {
      ID
      Items {
        QuestionID
      }
    }
  }
`

export const SEARCH_BY_URL = gql`
  mutation SearchByUrl($ImagePath: String!, $CourseGroupID: Int!) {
    SearchByUrl(ImagePath: $ImagePath, CourseGroupID: $CourseGroupID) {
      ID
      Items {
        QuestionID
      }
    }
  }
`

export const GET_SEARCH_BY_ID = gql`
  query GetUserSearchResultByID($ID: String!) {
    GetUserSearchResultByID(ID: $ID) {
      ID
      UserID
      Content
    	ContentLatex
      ImagePath
      CourseGroupID
      Items {
        IsRelevant
        IsLike
        Confidence
        Question {
          ID
          Content
          ImagePath
          QuestionCode
          SolutionVideoURL
          Metadata{
            Course{
              CourseTitle
            }
            Chapter{
              ChapterID
              ChapterTitle
            }
            Lesson{
              LessonID
              LessonTitle
            }
            Class {
              ClassID
              ClassTitle
            }
          }
          RelatedVideos{
            CourseTitle
            ChapterTitle
            LessonTitle
            VideoTitle
            Thumbnail
          }
        }
      }
    }
  }
`

export const UPLOAD_IMAGE = gql`
  mutation UploadImage($input: File!, $courseId: courseId!) {
    uploadImage(input: $input, courseId: $courseId) 
      @rest(type: "File", path: "/doubt-solving/upload?course_group_id={args.courseId}", method: "POST", bodySerializer: "fileEncode") {
        path
      }
  }
`;