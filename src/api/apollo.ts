import { ApolloClient, ApolloLink, HttpLink, InMemoryCache } from "@apollo/client";
import { setContext } from "@apollo/client/link/context";
import { onError } from "@apollo/client/link/error";
import { RestLink } from "apollo-link-rest";
import configs from "src/config/apiconfig";
import { PUBLIC_API_CLIENT, TOKEN_KEY } from "src/constants/keys";
import { getAuthBearer } from "src/utils/cookie";

const defaultHttpGraphqlLink = new HttpLink({ uri: configs?.apiGraphql })
const publicHttpGraphqlLink = new HttpLink({ uri: configs?.apiGraphqlPublic })

const restLink = new RestLink({
  uri: configs?.api,
  bodySerializers: {
    fileEncode: (data: any, headers: Headers) => {
      const formData = new FormData()
      formData.append('file', data, data.name)
      headers.set('Accept', '*/*')
      return { body: formData, headers }
    }
  }
})

const errorLink = onError(({ graphQLErrors, networkError }) => {
  if (graphQLErrors)
    graphQLErrors.forEach(({ message, locations, path }) =>
      console.log(
        `[GraphQL error]: Message: ${message}, Location: ${locations}, Path: ${path}`,
      ),
    );

  if (networkError) console.log(`[Network error]: ${networkError}`);
});

interface createApolloClient {
  tokenClient?: string | null;
}

export const createApolloClient = ({ tokenClient }: createApolloClient, path?: string) => {
  const authLink = setContext((_, { headers }) => {
    let token = tokenClient ? tokenClient : getAuthBearer(TOKEN_KEY, document.cookie);
    return {
      headers: {
        ...headers,
        authorization: token,
      }
    }
  });

  let publicLink = [restLink, errorLink, publicHttpGraphqlLink];
  let defaultLink = [authLink, restLink, errorLink, defaultHttpGraphqlLink];

  if (path) {
    const manualHttpLink = new HttpLink({ uri: `${configs?.api}${path}` });
    publicLink = [restLink, errorLink, manualHttpLink];
    defaultLink = [authLink, restLink, errorLink, manualHttpLink];
  }


  return new ApolloClient({
    ssrMode: typeof document === 'undefined',
    link: ApolloLink.split(
      operation => operation.getContext().clientName === PUBLIC_API_CLIENT,
      ApolloLink.from(publicLink),
      ApolloLink.from(defaultLink),
    ),
    cache: new InMemoryCache(),
  });
}
