import { gql } from "@apollo/client";

export const LOGIN = gql`
   mutation Login($input: LoginBody!) {
    login(input: $LoginBody) 
      @rest(type: "Login", path: "/auth/login", method: "POST") {
      message
      token
    }
  }
`;

export const GET_AUTH_CHECK = gql`
   query GetAuthCheck {
    authCheck
      @rest(type: "AuthCheck", path: "/auth/check") {
        id
        roles
        email
        message
    }
  }
`;