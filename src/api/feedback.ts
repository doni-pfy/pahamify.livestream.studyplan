import { gql } from "@apollo/client";

export const SUBMIT_SEARCH_FEEDBACK = gql`
mutation SubmitSearchFeedback($ID: String!, $IsRelevant: Boolean, $IsLike: Boolean) {
    SubmitSearchFeedback (
        ID: $ID
        IsRelevant: $IsRelevant
        IsLike: $IsLike
    ) {
        IsRelevant
    }
}
`

export const SUBMIT_SEARCH_QUESTION_FEEDBACK = gql`
mutation SubmitSearchQuestionFeedback($SearchID: String!, $QuestionID: String!, $IsRelevant: Boolean, $IsLike: Boolean) {
    SubmitSearchQuestionFeedback(
        SearchID: $SearchID
        QuestionID: $QuestionID
        IsRelevant: $IsRelevant
        IsLike: $IsLike
    ) {
        IsRelevant
        IsLike
    }
  }
`;