import { gql } from "@apollo/client";

export const GET_VIDEO_BY_QUESTION_ID = gql`
  query GetQuestionByID($ID: String!) {
    GetQuestionByID(ID: $ID) {
        ID
        Content
        SolutionVideoURL
        ImagePath
        Metadata {
            Course {
                CourseTitle
            }
            Chapter {
                ChapterTitle
            }
            Lesson {
                LessonTitle
            }
            Concept {
                ConceptTitle
            }
            Class {
                ClassTitle
            } 
        }
        RelatedVideos {
          VideoTitle
          Thumbnail
        }
    }
  }
`
