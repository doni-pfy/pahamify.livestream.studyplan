const env = process.env.ENV || 'development';
const configs = {
    development: {
        api: 'https://stg-mipi-api.pahamify.com',
        apiGraphql: 'https://stg-mipi-api.pahamify.com/ml-aggregator-v2/graphql',
        apiGraphqlPublic: 'https://stg-mipi-api.pahamify.com/ml-aggregator-v2/public/graphql',
        loginUrl: 'https://stg-login.pahamify.com',
        storageAssetPath: 'mipi-stg'
    },
    staging: {
        api: 'https://stg-mipi-api.pahamify.com',
        apiGraphql: 'https://stg-mipi-api.pahamify.com/ml-aggregator-v2/graphql',
        apiGraphqlPublic: 'https://stg-mipi-api.pahamify.com/ml-aggregator-v2/public/graphql',
        loginUrl: 'https://stg-login.pahamify.com',
        storageAssetPath: 'mipi-stg'
    },
    production: {
        api: 'https://api-mipi.pahamify.com',
        apiGraphql: 'https://api-mipi.pahamify.com/ml-aggregator-v2/graphql',
        apiGraphqlPublic: 'https://api-mipi.pahamify.com/ml-aggregator-v2/public/graphql',
        loginUrl: 'https://login.pahamify.com',
        storageAssetPath: 'mipi'
    },
}[env];

export default configs;