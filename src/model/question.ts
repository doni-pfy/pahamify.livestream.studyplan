import { Chapter } from "./chapter";
import { Concept } from "./concept";
import { Course } from "./course";
import { Lesson } from "./lesson";
import { Module } from "./module";
import { Class } from "./class";

export interface Question {
  ID: string
  QuestionCode: string
  Content: string
  ContentLatex: string
  SolutionVideoURL: string
  SolutionVideoUpdatedDate: string
  ImagePath: string
  Source: string
  Status1: string
  Status2: string
  Deadline1: string
  Deadline2: string
  IsDeadline1: Boolean
  IsDeadline2: Boolean
  RejectionReason: string
  RejectionFile: string
  Metadata: QuestionMetadata
}

export interface QuestionMetadata {
  CourseID: number
  Course: Course
  ChapterID: number
  Chapter: Chapter
  LessonID: number
  Lesson: Lesson
  ModuleID: number
  Module: Module
  ConceptID: number
  Concept: Concept
  ClassID: number
  Class: Class
}