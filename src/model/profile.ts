export interface Profile {
    profilePicture: string;
    bio: string;
    profile: studentProfile;
    searchHistory: SearchItem[];
}

export interface studentProfile {
    FullName: string;
    Class: string;
    School: string;
}

export interface SearchItem {
    ID: string
    UserID: number
    Content: string
    ContentLatex: string
    CourseGroupID: number
    ImagePath: string
    CreatedAt: Date
    BestConfidence: number
}