export interface Concept {
  ConceptID: number
  ConceptTitle: String
}