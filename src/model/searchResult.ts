import { Question } from "./question";

export interface SearchResult {
  ID: string
  UserID: number
  Content: string
  ContentLatex: string
  CourseGroupID: number
  ImagePath: string
  CreatedAt: Date
  Items: SearchQuestion[]
}

export interface SearchQuestion {
  SearchID: string
  QuestionID: string
  IsRelevant: Boolean
  IsLike: Boolean
  Question: Question
  Confidence: number
}