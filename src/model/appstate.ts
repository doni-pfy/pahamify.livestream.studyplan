export interface AppState {
    activeNav: string | null,
    pathToGo: string | null,
    isModalLoginOpen?: boolean
}

export const DefaultAppState: AppState = {
    activeNav: null,
    pathToGo: null,
    isModalLoginOpen: false
}

export enum AppStateActionType {
    SetActiveNav,
    SetModalLogin
}

export interface SetActiveNav {
    type: AppStateActionType.SetActiveNav;
    payload?: AppState;
}

export interface SetModalLogin {
    type: AppStateActionType.SetModalLogin;
    payload?: AppState;
}

export type AppStateActions = SetActiveNav | SetModalLogin;