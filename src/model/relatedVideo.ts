export interface RelatedVideo {
  CourseID: number
  ChapterID: number
  LessonID: number
  VideoID: number
  CourseTitle: String
  ChapterTitle: String
  LessonTitle: String
  VideoTitle: String
  Description: String
  Thumbnail: String
}