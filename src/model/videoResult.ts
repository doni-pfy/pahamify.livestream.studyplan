import { QuestionMetadata } from "./question";
import { RelatedVideo } from "./relatedVideo"

export interface VideoResult {
    ID: String
    Content: String
    QuestionCode: String
    SolutionVideoURL: String
    ImagePath: String
    Source: String
    Metadata: QuestionMetadata
    RelatedVideos: Array<RelatedVideo>
}