import { EMPTY_ARRAY, EMPTY_STRING } from "src/constants/empty";

export interface User {
    isLoggedIn: boolean,
    roles: number[],
    email: string,
    message: string,
    userId: number
}

export const DefaultUserData: User = {
    isLoggedIn: false,
    roles: EMPTY_ARRAY,
    email: EMPTY_STRING,
    message: EMPTY_STRING,
    userId: 0
}

export enum UserActionType {
    UpdateUser,
    DeleteUser,
    CheckUser
}

export interface UpdateUser {
    type: UserActionType.UpdateUser;
    payload: User;
}

export interface DeleteUser {
    type: UserActionType.DeleteUser;
    payload: User;
}

export interface CheckUser {
    type: UserActionType.CheckUser;
}

export type UserActions = UpdateUser | DeleteUser | CheckUser;