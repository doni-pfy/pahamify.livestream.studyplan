export type QuestionFeedback = {
  SearchID: String
  QuestionID: String
  IsRelevant: Boolean
  IsLike: Boolean
}

export type Answer = boolean | null;