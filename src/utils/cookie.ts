import { TOKEN_KEY } from "src/constants/keys";

export function setCookie(name: string, value: string, days: number) {
    var expires = "";
    if (days) {
        var date = new Date();
        date.setTime(date.getTime() + (days * 24 * 60 * 60 * 1000));
        expires = "; expires=" + date.toUTCString();
    }
    document.cookie = name + "=" + (value || "") + expires + "; path=/";
}


export function getAuthBearer(name: string, cookie: string | undefined = ''): string | null {
    const authInfo = parseCookie(TOKEN_KEY, cookie);
    if (!authInfo) return null;

    try {
        const token = JSON.parse(decodeURIComponent(authInfo)).auth.token;
        if (token === '') return null;
        return "bearer " + token;
    } catch (e) {
        console.log(e);
        return null;
    }
}

export function parseCookie(name: string, cookie: string | undefined = '') {
    var nameEQ = name + "=";
    var ca = cookie.split(';');
    for (var i = 0; i < ca.length; i++) {
        var c = ca[i];
        while (c.charAt(0) == ' ') c = c.substring(1, c.length);
        if (c.indexOf(nameEQ) == 0) return c.substring(nameEQ.length, c.length);
    }
    return null;
}

export function eraseCookie(name: string) {
    document.cookie = name + '=; Path=/; Expires=Thu, 01 Jan 1970 00:00:01 GMT;';
}