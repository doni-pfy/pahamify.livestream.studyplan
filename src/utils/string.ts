import { RESULT_DEFAULT_PATH } from "src/constants/url";
import { SearchItem } from "src/model/profile";

export const getIdSearch = (data: string = '', lengthId: number) => {
  const arrayData = data.split('-');
  return arrayData.slice(arrayData.length-lengthId).join('-');
}

export function removeSpecialChars(str: string) {
  return str.replace(/(?:[^a-zA-Z0-9]+)/g, '-');
}

export function buildSearchPath(searchItem: SearchItem): string {
  const basePath = searchItem.Content ? searchItem.Content : RESULT_DEFAULT_PATH;
  return `/result/${removeSpecialChars(basePath)}--${searchItem.ID}`;
}

export const convertToISODuration = (seconds: number) => {
  var hours = Math.floor(seconds / 3600);
  var minutes = Math.floor(seconds / 60);
  seconds = seconds % 60;
  return `PT${hours}H${minutes}M${seconds}S`;
}