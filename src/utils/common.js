export function secToMinutes(second) {
  var minutes = Math.floor(second / 60);
  var seconds = second - minutes * 60;

  if(minutes < 10){
    minutes = '0' + minutes;
  }
  if(seconds < 10){
    seconds = '0' + seconds;
  }

  return minutes + ':' + seconds;
}

export function secToHours(d, full) {
  d = Number(d);
  var h = Math.floor(d / 3600);
  var m = Math.floor(d % 3600 / 60);
  var s = Math.floor(d % 3600 % 60);

  if(full) {
    var hDisplay = h > 0 ? h + (h == 1 ? " Jam " : " Jam ") : "";
    var mDisplay = m > 0 ? m + (m == 1 ? " Menit " : " Menit ") : "";
    var sDisplay = s > 0 ? s + (s == 1 ? " Detik" : " Detik") : "";
  } else {
    var hDisplay = h > 0 ? h + (h == 1 ? " j " : " j ") : "";
    var mDisplay = m > 0 ? m + (m == 1 ? " m " : " m ") : "";
    var sDisplay = s > 0 ? s + (s == 1 ? " d" : " d") : "";
  }

  return hDisplay + mDisplay; 
}

export function convertTime(time) {
  const timesplit = time.split('+');
  let date = new Date(timesplit[0]);
  let n = date.getTimezoneOffset();
  
  date = new Date(date.getTime() - n * 60 * 1000);

  const now = new Date();
  const delta = Math.abs(now - date) / 1000;
  const mins = Math.floor(delta / 60) % 60;
  const hours = Math.floor(delta / 3600) % 24;
  const days = Math.floor(delta / 86400);
  if (mins <= 59 && hours < 1 && days < 1) {
    return `${mins} menit yang lalu`;
  } else if (hours <= 23 && days < 1) {
    return `${hours} jam yang lalu`;
  } else if (days <= 7) {
    return `${days} hari yang lalu`
  } else {
    return `${date.getDate()}-${date.getMonth()}-${date.getFullYear()}`
  }
}

export function convertTimeMili(time) {
  let date = new Date(time);
  let n = date.getTimezoneOffset();

  date = new Date(date.getTime() - n * 60 * 1000);

  const now = new Date();
  const delta = Math.abs(now - date) / 1000;
  const mins = Math.floor(delta / 60) % 60;
  const hours = Math.floor(delta / 3600) % 24;
  const days = Math.floor(delta / 86400);
  if (mins <= 59 && hours < 1 && days < 1) {
    return `${mins} menit yang lalu`;
  } else if (hours <= 23 && days < 1) {
    return `${hours} jam yang lalu`;
  } else if (days <= 7) {
    return `${days} hari yang lalu`
  } else {
    return `${date.getDate()}-${date.getMonth()}-${date.getFullYear()}`
  }
}

export function secondsToTime(secs) {
  const days = Math.floor(secs / 86400);

  const divisor_for_hours = secs % 86400;
  let hours = Math.floor(divisor_for_hours / (60 * 60));

  const divisor_for_minutes = secs % (60 * 60);
  let minutes = Math.floor(divisor_for_minutes / 60);

  const divisor_for_seconds = divisor_for_minutes % 60;
  let seconds = Math.ceil(divisor_for_seconds);

  if (seconds === 60) {
    seconds = 0;
    minutes += 1;
  } else if (minutes === 60) {
    minutes = 0;
    hours += 1;
  }

  const obj = {
    d: days,
    h: hours,
    m: minutes,
    s: seconds,
  };
  const time = `${obj.m < 10 ? '0'+obj.m : obj.m}:${obj.s < 10 ? '0'+obj.s : obj.s}`
  return time;
}

export function dateFormat() {
  const today = new Date();
  let monthNum = today.getMonth() + 1;
  monthNum = monthNum < 10 ? "0" + monthNum : monthNum;
  let dateNum = today.getDate();
  dateNum = dateNum < 10 ? "0" + dateNum : dateNum;
  const dateFormat = `${today.getFullYear()}${monthNum}${dateNum}`;

  return dateFormat;
}

export function getTodayDate () {
  const today = new Date();
  let month = "";
  switch (today.getMonth()) {
    case 0:
      month = "January";
      break;
    case 1:
      month = "February";
      break;
    case 2:
      month = "March";
      break;
    case 3:
      month = "April";
      break;
    case 4:
      month = "May";
      break;
    case 5:
      month = "June";
      break;
    case 6:
      month = "July";
      break;
    case 7:
      month = "August";
      break;
    case 8:
      month = "September";
      break;
    case 9:
      month = "October";
      break;
    case 10:
      month = "November";
      break;
    case 11:
      month = "December";
      break;
    case 6:
      month = "July";
      break;
    default:
      break;
  }
  return `${today.getDate()} ${month} ${today.getFullYear()}`;
}

export function toDataUrl(url) {
  return new Promise((resolve, reject) => {
    var xhr = new XMLHttpRequest();
    xhr.onload = function() {
        var reader = new FileReader();
        reader.onloadend = function() {
            // callback(reader.result);
            resolve(reader.result);
        }
        reader.readAsDataURL(xhr.response);
    };
    xhr.open('GET', url + "?" + new Date().getTime() );
    xhr.responseType = 'blob';
    xhr.send();
  })
}

export function compareByDate(a, b) {
  // Use toUpperCase() to ignore character casing
  const createdAtA = a.createdAt.toUpperCase();
  const createdAtB = b.createdAt.toUpperCase();

  let comparison = 0;
  if (createdAtA > createdAtB) {
    comparison = 1;
  } else if (createdAtA < createdAtB) {
    comparison = -1;
  }
  return comparison;
}




