import { LabelConfidence } from "src/constants/label";
import { Subject } from "src/constants/subject";

export const getSubjectName = (subjectId: number) => {
  switch (subjectId) {
    case Subject.Matematika.Id:
      return Subject.Matematika.Text;
    case Subject.Fisika.Id:
      return Subject.Fisika.Text;
    case Subject.Kimia.Id:
      return Subject.Kimia.Text;
    default:
      return "";
  }
};

export const getConfidenceLabel = (confidence: number) => {
  if (confidence >= LabelConfidence.SangatMirip.threshold) {
    return ({
      label: LabelConfidence.SangatMirip.text,
      className: "success",
    })
  } else if (
    LabelConfidence.CukupMirip.threshold <= confidence &&
    confidence < LabelConfidence.SangatMirip.threshold
  ) {
    return({
      label: LabelConfidence.CukupMirip.text,
      className: "warning",
    });
  } else if (
    LabelConfidence.MungkinMirip.threshold <= confidence &&
    confidence < LabelConfidence.CukupMirip.threshold
  ) {
    return({
      label: LabelConfidence.MungkinMirip.text,
      className: "danger",
    });
  } else {
    return(null);
  }
};