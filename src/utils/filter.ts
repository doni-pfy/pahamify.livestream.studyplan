import { SearchQuestion } from "src/model/searchResult";
import { FilterType } from "src/types/resultFilterType";

export const getAllBab = (data: SearchQuestion[]) => {
  let temp: FilterType[] = [];
  data.forEach((item) => {
    const Metadata = item.Question.Metadata;
    if (temp.length == 0) {
      temp.push({
        id: Metadata.Chapter.ChapterID,
        text: Metadata.Chapter.ChapterTitle,
      });
    } else {
      const tempLength = temp.length;
      let unique = true;
      for (let i = 0; i < tempLength; i++) {
        if (temp[i].id === Metadata.Chapter.ChapterID) {
          unique = false;
          break;
        }
      }

      if (unique)
        temp.push({
          id: Metadata.Chapter.ChapterID,
          text: Metadata.Chapter.ChapterTitle,
        });
    }
  });
  return temp;
};

export const getAllKelas = (data: SearchQuestion[]) => {
  let temp: FilterType[] = [];
  data.forEach((item) => {
    const Metadata = item.Question.Metadata;
    if (temp.length == 0) {
      temp.push({
        id: Metadata.Class.ClassID,
        text: Metadata.Class.ClassTitle,
      });
    } else {
      const tempLength = temp.length;
      let unique = true;
      for (let i = 0; i < tempLength; i++) {
        if (temp[i].id === Metadata.Class.ClassID) {
          unique = false;
          break;
        }
      }

      if (unique)
        temp.push({
          id: Metadata.Class.ClassID,
          text: Metadata.Class.ClassTitle,
        });
    }
  });
  return temp;
};