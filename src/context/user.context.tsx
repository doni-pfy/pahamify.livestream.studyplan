import { useLazyQuery } from '@apollo/client'
import * as React from 'react'
import { GET_AUTH_CHECK } from 'src/api/rest'
import { EMPTY_STRING } from 'src/constants/empty'
import { AUTH_API_CLIENT } from 'src/constants/keys'
import { DefaultUserData, User, UserActions, UserActionType } from 'src/model/user'

type Dispatch = (action: UserActions) => void
type State = { user: User, isLoading: boolean }
type UserProviderProps = { children: React.ReactNode }

const UserStateContext = React.createContext<{
    state: State; dispatch: Dispatch
} | undefined>
    (undefined)

function userReducer(state: State, action: UserActions) {
    switch (action.type) {
        case UserActionType.UpdateUser:
            return {
                user: {
                    ...state.user,
                    isLoggedIn: action.payload.isLoggedIn,
                    roles: action.payload.roles,
                    email: action.payload.email,
                    message: action.payload.message,
                    userId: action.payload.userId,
                },
                isLoading: false,
            }
        case UserActionType.DeleteUser:
            return {
                user: {
                    ...state.user,
                    isLoggedIn: false,
                    email: EMPTY_STRING,
                    userId: 0,
                },
                isLoading: state.isLoading,
            }
        case UserActionType.CheckUser:
            return {
                user: state.user,
                isLoading: true,
            }
    }
}

function UserProvider({ children }: UserProviderProps) {
    const [state, dispatch] = React.useReducer(userReducer, {
        user: DefaultUserData,
        isLoading: true,
    })

    const [checkauth, { data }] = useLazyQuery(GET_AUTH_CHECK, {
        context: { clientName: AUTH_API_CLIENT },
        onError: (e) => console.log(e.message)
    })
    const value = { state, dispatch }

    React.useEffect(() => {
        if (data) {
            dispatch({
                type: UserActionType.UpdateUser,
                payload: {
                    isLoggedIn: true,
                    email: data.authCheck.email,
                    roles: data.authCheck.roles,
                    message: data.authCheck.message,
                    userId: data.authCheck.id,
                }
            })
        }
    }, [data])

    React.useEffect(() => {
        if (!state.user.isLoggedIn) {
            dispatch({ type: UserActionType.CheckUser });
            checkauth();
        }
    }, [])

    return (
        <UserStateContext.Provider value={value}>
            {children}
        </UserStateContext.Provider>
    )
}

function useUser() {
    const context = React.useContext(UserStateContext)
    if (context === undefined) {
        throw new Error('useUser must be used within a UserProvider')
    }
    return context
}

export { UserProvider, useUser }

