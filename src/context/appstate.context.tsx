import { useRouter } from 'next/router'
import * as React from 'react'
import { TAB_MENU } from 'src/constants/menu'
import { Path } from 'src/constants/path'
import { AppState, AppStateActions, AppStateActionType, DefaultAppState } from 'src/model/appstate'

type Dispatch = (action: AppStateActions) => void
type State = { appState: AppState }
type AppStateProviderProps = { children: React.ReactNode }

const AppStateContext = React.createContext<{
    state: State; dispatch: Dispatch
} | undefined>
    (undefined)

function appStateReducer(state: State, action: AppStateActions) {
    switch (action.type) {
        case AppStateActionType.SetActiveNav:
            return {
                appState: {
                    ...state.appState,
                    activeNav: action.payload!!.activeNav,
                    pathToGo: action.payload!!.pathToGo
                }
            }
        case AppStateActionType.SetModalLogin:
            return {
                appState: {
                    ...state.appState,
                    isModalLoginOpen: action.payload!!.isModalLoginOpen
                }
            }

    }
}

function AppStateProvider({ children }: AppStateProviderProps) {
    const [state, dispatch] = React.useReducer(appStateReducer, {
        appState: DefaultAppState
    })
    const value = { state, dispatch };
    const router = useRouter();

    React.useEffect(() => {
        const pathToGo = state.appState.pathToGo;
        if ((pathToGo !== null) && (pathToGo !== location.pathname)) {
            state.appState.pathToGo && router.push(state.appState.pathToGo);
        }

        if (state.appState.activeNav === null) {
            syncWithUrl(location.pathname);
        }
    }, [state.appState])

    React.useEffect(() => {
        initiateAppStateSyncListener();
    }, [])

    const initiateAppStateSyncListener = () => {
        router.beforePopState(({ url }) => {
            syncWithUrl(url);
            return true;
        })
    }

    const syncWithUrl = (url: string) => {
        switch (url) {
            case (url.match(Path.Forum) || {}).input:
                dispatch({ type: AppStateActionType.SetActiveNav, payload: { activeNav: TAB_MENU.FORUM, pathToGo: location.pathname } })
                break;
            case (url.match(Path.Profile) || {}).input:
                dispatch({ type: AppStateActionType.SetActiveNav, payload: { activeNav: TAB_MENU.PROFILE, pathToGo: location.pathname } })
                break;
            default:
                dispatch({ type: AppStateActionType.SetActiveNav, payload: { activeNav: TAB_MENU.FOTO, pathToGo: location.pathname } })
                break;
        }
    }

    return (
        <AppStateContext.Provider value={value}>
            {children}
        </AppStateContext.Provider>
    )
}

function useAppState() {
    const context = React.useContext(AppStateContext)
    if (context === undefined) {
        throw new Error('useAppState must be used within a AppStateProvider')
    }
    return context
}

export { AppStateProvider, useAppState }

