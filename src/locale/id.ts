export const intlMessagesId = {
    'app.name': 'MIPI',

    'card.title.ask.mipi': 'Tanya MIPI',
    'card.title.ask.mipi.description': 'Temukan jawaban akuratmu di Mipi',
    'button.text.upload.photo': 'Ambil Foto',
    'text.how.to.use.feature': 'Cara Foto Soal',
    'subject.math': 'Matematika',
    'subject.physics': 'Fisika',
    'subject.chemistry': 'Kimia',
    'subject.saintek': 'Saintek',
    'subject.soshum': 'Soshum',
    'subject.campuran': 'Campuran',
    'subject.choose': 'Pilih Pelajaran',
    'major.TPS': 'TPS',
    'major.Soshum': 'Soshum',
    'major.Saintek': 'Saintek',
    'subject.dropdown.error': 'Pilih salah satu mata pelajaran',

    'card.title.kesempatan.habis': 'Login Dulu, untuk lanjut bertanya!',
    'card.text.kesempatan.habis': 'Kamu bisa Tanya Mipi sepuasnya setelah login lho!{br}{br} {note} Kamu hanya bisa login dengan akun Pahamify',
    'button.text.kesempatan.habis': 'Login Dengan Akun Pahamify',
    'button.second.text.kesempatan.habis': 'Buat Akun Pahamify',

    'card.title.contoh.soal': 'Coba Contoh Soal Mipi',
    'card.text.contoh.soal': 'Kamu belum punya soal? Kamu bisa melihat contoh solusi dari Mipi untuk mencoba penggunaan Tanya Mipi',
    'button.text.contoh.soal': 'Gunakan Soal Contoh',

    'card.title.login': 'Login Dulu, Yuk!',
    'card.text.login': 'Untuk mulai menggunakan MIPI, kamu harus login dulu, ya!{br}{br} {note} Kamu hanya bisa login dengan akun Pahamify',
    'card.text.profile.login': 'Halaman Profil bisa kamu akses semudah login akun Pahamify!',
    'button.text.login': 'Login Dengan Akun Pahamify',
    'button.second.text.login': 'Buat Akun Pahamify',

    'card.title.answer.notfound': 'Jawaban Tidak Ditemukan',
    'card.text.answer.notfound': 'Maaf ya, Mipi belum bisa menjawab soalmu. Yuk coba kita tanya ke Forum!',
    'button.text.answer.notfound': 'Tanyakan Soal Ke Forum',
    'button.text.second.answer.notfound': 'Tanya Soal Lain',

    'card.title.failed.upload': 'Maaf, Foto Gagal Diunggah',
    'card.text.failed.upload': 'Pastikan internet kamu terhubung dan silakan coba kembali.',
    'button.text.failed.upload': 'Coba Lagi',
    'button.text.back': 'Kembali',

    'button.text.login.header': 'Login',
    'button.text.studyplan.header': 'Study Plan',
    'footer.text': 'Pahamify. All rights reserved.',

    'page.title.guide': 'Cara Foto Soal',
    'page.title.ask.mipi': 'Tanya Mipi',
    'page.title.edit.photo': 'Edit Foto',
    'page.title.login': 'Masuk Mipi',
    'page.title.faq': 'FAQ',
    'page.title.service.terms': 'Persyaratan Layanan',
    'page.title.privacy.policy': 'Kebijakan Privasi',
    'page.title.profile.picture.take': 'Ambil Foto',
    'page.title.profile.bio.edit': 'Edit Bio',
    'page.title.account': 'Pengaturan Akun Pahamify',

    'section.title.feature': 'Keunggulan',
    'section.title.testimoni': 'Apa Kata Mereka',

    'home.page.heading.title': 'Pahamify Study Plan!',
    'home.page.heading.description': 'Hanya dengan kamera ponselmu, bisa dapat jawaban soal mapel apa saja secara instan!',

    'login.button': 'Masuk',
    'login.info': 'Kamu juga bisa masuk dengan email dan password akun Pahamify',
    'login.error': 'Email dan password kamu tidak sesuai',

    'section.title.type.question': 'Mipi, Tanya Dong!',
    'section.text.type.question': 'Silakan tulis soal yang ingin kamu tanyakan dengan jelas dan sopan di kolom berikut.',
    'placeholder.text.type.question': 'Tuliskan soalmu di sini ...',
    'error.text.type.question': 'Kamu belum menuliskan soal apapun!',

    'button.try.question': 'Coba dulu pake soal dari Pahamify?',
    'button.camera': 'Kamera',
    'button.type.question': 'Ketik Soal',
    'button.type.question.submit': 'Tanyakan',
    'button.finish.edit.image': 'Selesai',

    'separator.or': 'atau',
    'separator.login': 'Belum punya akun?',

    'section.title.subbab.terkait': 'Pelajari Materi Terkait di Pahamify',
    'page.title.detail.pembahasan': 'Detail Pembahasan',

    'text.chapter': 'Bab',
    'text.lesson': 'Subbab',
    'button.load.more': 'Tampilkan Lebih Banyak',
    'button.ask.forum': 'Tanya ke Forum',

    'loading.mipi': 'Mipi merenung sejenak...',
    'loading.upload': 'Fotomu sedang diunggah...',
    'loading.button': 'Memuat...',

    'label.empty': '-',
    'label.mirip': 'Sangat Mirip',
    'label.cukup.mirip': 'Cukup Mirip',
    'label.mungkin.mirip': 'Mungkin Mirip',

    'button.back': 'Kembali',
    'button.save': 'Simpan',

    'data.not.found': 'Data Tidak Ditemukan.',
    'checkbox.select.all': 'Pilih Semua',

    'text.match.question.feedback': 'Hai kamu! Apakah hasil pencarian kamu sudah sesuai dengan yang kamu inginkan?',
    'text.understand.question.feedback': 'Apakah kamu paham dengan pembahasannya?',
    'text.yes.feedback': 'Ya',
    'text.no.feedback': 'Tidak',
    'text.send.feedback': 'Kirim',
    'text.next.time.feedback': 'Lain Kali',

    'card.title.to.forum.feedback': 'Tanyakan ke Forum, Yuk!',
    'card.text.to.forum.feedback': 'Kamu bisa mendapatkan jawaban yang lebih lengkap dari teman-teman lain atau Rockstar Teacher!',
    'button.to.forum.feedback.submit': 'Tanyakan Soal ke Forum',

    'button.edit.profile': 'Pengaturan Akun Pahamify',
    'button.edit.profile.bio': 'Edit Bio',
    'placeholder.text.type.bio': 'Masukkan bio di sini ...',
    'error.text.type.bio': 'Maksimal panjang bio 255 huruf yaa',
    'label.text.type.bio': 'Bio',
    'button.type.bio.submit': 'Simpan',
    'card.title.failed.upload.type.bio': 'Maaf, Data Gagal Disimpan',

    'button.edit.profile.profile': 'Ubah Profil',
    'button.edit.profile.password': 'Ubah Kata Sandi',
    'button.type.logout.submit': 'Keluar',

    'label.question.and.result': 'Soal dan hasil jawaban',
    'label.search.history': 'Riwayat Pencarian',

    'about.us': 'Tentang Kami',
    'faq': 'FAQ',
    'service.terms': 'Persyaratan Layanan',
    'privacy.policy': 'Kebijakan Privasi',

    'text.perhitungan.waktu': 'Perhitungan waktu Hutang Belajar merupakan perkiraan, dengan asumsi tiap hari kamu belajar dengan efektif '
};
