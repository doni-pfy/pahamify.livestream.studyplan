import Link from "next/link";
import Camera from "public/icon/camera.svg";
import Profile from "public/icon/profile.svg";
import React, { useState } from "react";
import { TAB_MENU } from "src/constants/menu";
import { Path } from "src/constants/path";
import { useAppState } from "src/context/appstate.context";
import { useUser } from "src/context/user.context";
import { AppStateActionType } from "src/model/appstate";
import styles from "src/styles/BottomNav.module.scss";
import ImageCircle from "../elements/ImageCircle";
import LoginModal from "./LoginModal";

const BottomNav = () => {
  const {
    state: { appState },
    dispatch: dispatchAppState,
  } = useAppState();
  const {
    state: { user },
  } = useUser();
  const [showModalLogin, setShowModalLogin] = useState<boolean>(false);

  const changeActiveNav = (tab: string, pathToGo: string) => {
    dispatchAppState({
      type: AppStateActionType.SetActiveNav,
      payload: { activeNav: tab, pathToGo: pathToGo },
    });
  };

  const onProfileClicked = () => {
    if (user.isLoggedIn) {
      changeActiveNav(TAB_MENU.PROFILE, Path.Profile);
    } else {
      setShowModalLogin(true);
    }
  };

  return (
    <>
      <div
        className={`container fixed bottom-0 flex justify-around bg-white w-full rounded-t-lg z-10 ${styles["container"]}`}
      >
        <Link href="/">
          <a
            onClick={() => changeActiveNav(TAB_MENU.FOTO, Path.Root)}
            className={`font-bold text-xs pt-2.5 pb-2 px-1.5 border-0 border-solid border-b-2 flex-grow flex-1 text-center 
              ${
                appState.activeNav === TAB_MENU.FOTO
                  ? styles["menu-active"]
                  : styles["menu"]
              }`}
          >
            <Camera className="mx-auto mb-0.5" />
            <span>Foto Soal</span>
          </a>
        </Link>
        {/* <Link href="/">
          <a
            onClick={() => setActiveNav(TAB_MENU.FORUM)}
            className={`font-bold text-xs pt-2.5 pb-2 px-1.5 border-0 border-solid border-b-2 flex-grow flex-1 text-center ${
              activeNav === TAB_MENU.FORUM
                ? styles["menu-active"]
                : styles["menu"]
            }`}
          >
            <TextPaper className="mx-auto mb-0.5" />
            <span>Forum</span>
          </a>
        </Link> */}
        <a
          onClick={onProfileClicked}
          className={`font-bold text-xs pt-2.5 pb-2 px-1.5 border-0 border-solid border-b-2 flex-grow flex-1 text-center cursor-pointer ${
            appState.activeNav === TAB_MENU.PROFILE
              ? styles["menu-active"]
              : styles["menu"]
          }`}
        >
          {user.isLoggedIn ? (
            <ImageCircle
              className="mb-0.5"
              url="/empty-profile.png"
              alt="profile picture"
            />
          ) : (
            <Profile className="mx-auto mb-0.5" />
          )}
          <span>Akun</span>
        </a>
      </div>
      {showModalLogin && (
        <LoginModal
          title="card.title.login"
          desc="card.text.profile.login"
          separatorText="separator.login"
          onClose={() => setShowModalLogin(false)}
        />
      )}
    </>
  );
};

export default BottomNav;
