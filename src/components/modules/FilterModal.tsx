import React, { useEffect, useState } from "react";
import { DataFilterType } from "src/types/resultFilterType";
import { GeneralButton } from "../elements/GeneralButton";
import FilterModalSection from "./FilterModalSection";

interface FilterModalProps {
  onClose: () => void;
  onSubmit: (data: any) => void;
  data: DataFilterType[];
}

const FilterModal = (props: FilterModalProps) => {
  const { onClose, data, onSubmit } = props;
  const [filters, setFitlers] = useState<any>({});
  const [changedData, setChangedData] = useState<boolean>(false);
  const [resetData, setResetData] = useState<boolean>(false);

  useEffect(() => {
    data.forEach((item) => {
      filters[item.label] = item.filtered;
    });
  }, []);

  const handleChangeFilter = (dataFilter: Array<number>, label: string) => {
    setChangedData(true);
    filters[label] = dataFilter;
  };

  const renderFilter = () => {
    return data.map((item, idx) => (
      <FilterModalSection
        key={idx}
        item={item}
        onchange={(dataFilter) => {
          handleChangeFilter(dataFilter, item.label);
        }}
        isReset={resetData}
        setResetData={setResetData}
      />
    ));
  };
  
  return (
    <>
      <div className="container fixed w-full h-screen top-0 left-1/2 transform -translate-x-1/2 z-20 animate-fade-in">
        <div
          className="w-full h-full opacity-90"
          style={{ backgroundColor: "#E0E8F3" }}
          onClick={onClose}
        ></div>
      </div>
      <div className="container fixed w-full bottom-0 z-30 left-1/2 transform -translate-x-1/2">
        <div className="animate-fade-in-up">
          <div className="pt-7 px-4 pb-3 rounded-t-lg bg-base-tertiary flex items-center justify-between border-b-2 border-solid border-base-primary border-opacity-10">
            <div className="text-text-primary font-bold">Filter</div>

            <a className="text-sm font-bold text-base-primary cursor-pointer" onClick={() => setResetData(true)}>
              Reset
            </a>
          </div>
          <div className="pt-2 px-4 pb-4 bg-base-tertiary -mb-2">
            {renderFilter()}
          </div>
          <div className="p-4 bg-white rounded-t-lg">
            {!changedData ? (
              <GeneralButton
                className="bg-white border-base-primary border-solid border rounded-full"
                textId="button.back"
                textColor="text-text-primary"
                onClick={onClose}
              />
            ) : (
              <GeneralButton
                className="bg-base-primary border-base-primary border-solid border rounded-full"
                textId="button.save"
                onClick={() => onSubmit(filters)}
              />
            )}
          </div>
        </div>
      </div>
    </>
  );
};

export default FilterModal;
