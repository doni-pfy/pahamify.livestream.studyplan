import React, { useRef } from "react";
import { GeneralButton } from "../elements/GeneralButton";
import { TAB_MENU } from "src/constants/menu";

import CameraFlip from "public/icon/camera-flip.svg";
import Folder from "public/icon/folder.svg";

interface NavTanyaMipiProps {
  active: String;
  handleChangeNav: (activeNav: string) => void;
  handleSubmit: () => void;
  handleFlipCamera: () => void;
  handleUpload: (selectedFile: File) => void;
}

const NavTanyaMipi = (props: NavTanyaMipiProps) => {
  const { active, handleChangeNav, handleSubmit, handleFlipCamera, handleUpload } = props;
  const inputFileUpload = useRef<HTMLInputElement>(null);

  function loadFile() {
    if (!inputFileUpload.current || !inputFileUpload.current.files) return;
    var filesSelected = inputFileUpload.current.files;

    if (filesSelected.length > 0) {
      handleUpload(filesSelected[0]);
    }
  }

  return (
    <div className="container fixed bottom-0 w-full z-50 bg-text-primary pb-6 pt-4">
      {active === TAB_MENU.MIPI_CAMERA ? (
        <div className="px-4 pb-4 flex items-center justify-around">
          <label htmlFor="upload">
            <div
              className={`rounded-full cursor-pointer border-2 border-white flex items-center justify-center`}
              style={{ width: "40px", height: "40px" }}
            >
              <Folder/>
            </div>
            <input type="file" ref={inputFileUpload} id="upload" onChange={loadFile} accept="image/*" className="hidden"></input>
          </label>
          <div
            onClick={handleSubmit}
            className={`rounded-full cursor-pointer border-4 border-white bg-base-primary`}
            style={{ width: "64px", height: "64px" }}
          >
            {/* Submit */}
          </div>
          <div
            onClick={handleFlipCamera}
            className={`rounded-full cursor-pointer border-2 border-white flex items-center justify-center`}
            style={{ width: "40px", height: "40px" }}
          >
            <CameraFlip />
          </div>
        </div>
      ) : (
        <div className="pt-2 px-4 pb-9">
          <GeneralButton
            onClick={() => handleSubmit()}
            textId="button.type.question.submit"
            className="bg-white rounded-full text-center"
            textColor="text-base-primary"
          />
        </div>
      )}

      <div className="flex items-center justify-center">
        <GeneralButton
          onClick={() => handleChangeNav(TAB_MENU.MIPI_CAMERA)}
          textId="button.camera"
          size="sm"
          className={`mx-1 ${active === TAB_MENU.MIPI_CAMERA ? "bg-white rounded-full" : ""
            }`}
          textColor={`${active === TAB_MENU.MIPI_CAMERA ? "text-base-primary" : "text-white"
            }`}
        />

        <GeneralButton
          onClick={() => handleChangeNav(TAB_MENU.MIPI_TYPE_QUESTION)}
          textId="button.type.question"
          size="sm"
          className={`mx-1 ${active === TAB_MENU.MIPI_TYPE_QUESTION
            ? "bg-white rounded-full"
            : ""
            }`}
          textColor={`${active === TAB_MENU.MIPI_TYPE_QUESTION
            ? "text-base-primary"
            : "text-white"
            }`}
        />
      </div>
    </div>
  );
};

export default NavTanyaMipi;
