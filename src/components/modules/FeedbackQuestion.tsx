import React, { useState } from "react";
import { FormattedMessage } from "react-intl";

import HappyIcon from "public/icon-feedback/happy.svg";
import AngryIcon from "public/icon-feedback/angry.svg";
import WhiteHappyIcon from "public/icon-feedback/happy-white.svg";
import WhiteAngryIcon from "public/icon-feedback/angry-white.svg";

import { Answer } from "src/model/questionFeedback";

type FeedbackQuestionProps = {
    questionId: string,
    className?: string,
    onChange: (c: Answer) => void,
    hasError?: boolean
}

const styles = "flex gap-3 items-center justify-center p-4 rounded-xl border-2";

export const FeedbackQuestion = (props: FeedbackQuestionProps) => {
    const { questionId, className, onChange, hasError=false } = props;
    const [answer, setAnswer] = useState<Answer>(null);

    const selectAnswer = (choice: Answer) => {
        setAnswer(choice);
        onChange(choice);
    };
    
    // checking answer written in answer === false to avoid confusion with null
    return (
        <div className={`${className}`}>
            <FormattedMessage id={questionId}>
                {(message) => <p className="text-base leading-6 pb-3">{message}</p>}
            </FormattedMessage>
            <div className="grid grid-cols-2 justify-items-stretch gap-4">
                <div className={`${styles} ${answer === false ? "bg-red" : "border-opacity-20"} border-red cursor-pointer`} onClick={() => selectAnswer(false)}>
                    { answer === false ? <WhiteAngryIcon /> : <AngryIcon /> }
                    <FormattedMessage id="text.no.feedback">
                        {(message) => <p className={`${answer === false ? "text-white": "text-red"} leading-6`}>{message}</p>}
                    </FormattedMessage>
                </div>
                <div className={`${styles} ${answer === true ? "bg-green" : "border-opacity-20"} border-green cursor-pointer`} onClick={() => selectAnswer(true)}>
                    { answer === true ? <WhiteHappyIcon /> : <HappyIcon /> }
                    <FormattedMessage id="text.yes.feedback">
                        {(message) => <p className={`${answer === true ? "text-white": "text-green"} leading-6`}>{message}</p>}
                    </FormattedMessage>
                </div>
            </div>
            {
              hasError && <p className="text-red text-sm italic self-start mt-2">Pilih salah satu</p>
            }
        </div>
    )
}