import { useRouter } from 'next/router';
import BackArrow from "public/icon/icon-arrow-back.svg";
import React from 'react';
import styles from "src/styles/Header.module.scss";


export const TransparentHeader = () => {
    const router = useRouter();

    return (
        <nav className="pt-4 pl-4 fixed cursor-pointer z-50">
            <a onClick={() => router.back()}>
                <BackArrow className={`w-8 h-8 ${styles["icon-logo-white"]}`} />
            </a>
        </nav>
    );
};
