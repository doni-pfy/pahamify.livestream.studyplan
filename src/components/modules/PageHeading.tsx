import React, { ReactNode } from "react";
import { GroupDropDown } from "../elements/GroupDropdown";
import { SubjectDropdown } from "../elements/SubjectDropdown";

interface PageHeadingProps {
  title?: string;
  description?: string;
  children?: ReactNode;
  className?: string;
  hasBackground?: boolean;
  onSelectJurusan: (subjectId: number) => (void)
}

const PageHeading = (props: PageHeadingProps) => {
  const { title, description, children, className, hasBackground = true, onSelectJurusan } = props;

  return (
    <div className={`${className} bg-base-primary`}>
      <div className="container px-4 mt-5 mb-20">
        <div className="px-4">
          <div className="border-2 rounded-lg border-white border-opacity-20">
            <h1 className="text-center text-lg font-bold ls-0.025 text-white">Hitung Waktu Hutang Belajarmu untuk <span className="text-yellow">Taklukkan UTBK</span> Disini!</h1>
          </div>

          <div className=" mt-8 flex items-center justify-between">
            <p className="text-white opacity-80">Pilih Jurusan</p>
            <GroupDropDown onSelected={onSelectJurusan} />
          </div>
        </div>
      </div>
    </div>
  );
};

export default PageHeading;
