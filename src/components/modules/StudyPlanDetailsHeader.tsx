import React, { useState } from "react";
import { useRouter } from "next/router";
import InfoIcon from 'public/info.svg';
import { StudyPlanDetailsSectionHeader } from "../elements/StudyPlanDetailsSectionHeader";
import { StudyPlanDetailsHeaderTitle } from "../elements/StudyPlanDetailsHeaderTitle"
import { SubjectDropdown } from "../elements/SubjectDropdown";
import { TextWithIcon } from "../elements/TextWithIcon";
import { FormattedMessage } from "react-intl";
import { MipiChances, MipiSubjectId } from "src/constants/windowStorage";
import { useUser } from "src/context/user.context";
import { GeneralButton } from "../elements/GeneralButton";
import Camera from "public/icon/camera.svg";
import LoginModal from "./LoginModal";

interface StudyPlanDetailsHeaderProps {
    onClick?: () => void,
    routerPath?: string,
    className: string;
}

export const StudyPlanDetailsHeader = (props: StudyPlanDetailsHeaderProps) => {
    const { onClick, routerPath, className } = props;
    const [modalKesempatanOpen, setModalKesempatanOpen] = React.useState<Boolean>(false);
    const [subjectId, setSubjectId] = useState<number>(0);
    const [errorSubject, setErrorSubject] = useState<boolean>(false);
    const router = useRouter();
    const {state: {user}} = useUser()

    const chapter = router.query.chapter

    if(!chapter) { return (
      <div></div>
    )}

    const chapterData = JSON.parse(chapter as string)

     console.log('chapterData', chapterData)

    const handleSubmit = () => {
      if (!subjectId) {
        setErrorSubject(true);
        return;
      }
      setErrorSubject(false);
      const CHANCES = window.localStorage.getItem(MipiChances) ? Number(window.localStorage.getItem(MipiChances)):3;

      if (CHANCES <= 0 && !user.isLoggedIn) {
        setModalKesempatanOpen(true);
        return;
      }
      
      window.sessionStorage.setItem(MipiSubjectId, subjectId.toString());
      router.push('/tanya-mipi');
    }

    const handleSelectSubject = (subjectId: number) => {
      setSubjectId(subjectId)
    }

    return (
        <div className={`${className} p-4 flex flex-col bg-white rounded-lg`} style={{boxShadow: '0px 5px 20px rgba(59, 115, 197, 0.05)'}}>
            <div className="flex flex-row">
              <StudyPlanDetailsHeaderTitle title={chapterData.bigChapterTitle} descriptionId='card.title.ask.mipi.description'/>
              <StudyPlanDetailsSectionHeader title={chapterData.bigChapterTitle} progress={chapterData.progress} descriptionId='card.title.ask.mipi.description'/>
            </div>
        </div>
    )
}