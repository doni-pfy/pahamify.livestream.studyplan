import React from 'react';
import { FormattedMessage } from 'react-intl';
import RightArrow from 'public/carret-right.svg';
import { RelatedVideo } from 'src/model/relatedVideo';
import { BASE_STORAGE } from 'src/constants/url';

interface MateriTerkaitProps {
    lesson: string,
    course: string,
    courseClass: string,
    videoList: Array<RelatedVideo>,
}

export const CardMateriTerkait = (props: MateriTerkaitProps) => {
    const { lesson, course, courseClass, videoList } = props;
    const DOWNLOAD_LINK = "https://pahamify.com/download/";

    const showList = (videoList: RelatedVideo) => {
        return (
            <a href={DOWNLOAD_LINK} key={videoList.Thumbnail.valueOf()} target="_blank">
                <div className="flex items-center pt-5">
                    <img className={`w-24 rounded`} src={`${BASE_STORAGE}${videoList.Thumbnail}`} />
                    <span className="pl-4 font-bold text-base-primary w-50">
                        {videoList.VideoTitle}
                    </span>
                </div>
            </a>
        );
    }

    return (
        <div className="mx-4 mt-8 mb-6">
            <FormattedMessage id="section.title.subbab.terkait">
                {(message) => (
                    <div className="font-bold text-text-primary mb-6">
                        {message}
                    </div>
                )}
            </FormattedMessage>

            <div className="bg-white p-4 rounded-lg shadow-md">
                <a href={DOWNLOAD_LINK} target="_blank">
                    <div className="flex items-center justify-between pb-3 border-b-2 border-base-secondary border-opacity-10">
                        <div>
                            <div className="font-bold text-text-primary">
                                {lesson}
                            </div>
                            <div className="mt-1 text-sm font-bold text-text-primary text-opacity-60 ">
                                {course}  •  {courseClass}
                            </div>
                        </div>
                        <div className="p-2">
                            <RightArrow/>
                        </div>
                    </div>
                </a>
                <div className="flex flex-col">
                    {videoList? videoList.map((v, i, a) => showList(v)) : null}
                </div>
            </div>
        </div>
    )
}