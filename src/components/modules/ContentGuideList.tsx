import React, { useState } from 'react';
import styles from 'src/styles/guide.module.scss'

import { GeneralIcon } from 'src/components/elements/GeneralIcon';

interface ContentGuideProps {
    datas: any[],
}

const ContentGuide = (props:ContentGuideProps) => {
    const { datas } = props;

    return (
        <div className="bg-base-tertiary rounded-t-lg p-4 min-h-screen">
            { datas.map((data: any, index: any) => 
                <div key={index} className={`${index === datas.length-1? 'pb-16' : styles['guide-border']} flex items-start justify-content-center py-4 w-full`}>
                    <div className={`${styles['number-circle']} text-center`}>
                        <span className="font-bold text-sm text-white">{index+1}</span>
                    </div>
                    <div className="px-4 text-left">
                        <span className="font-normal text-sm text-text-primary">{data.textId}</span>
                        <div className="flex items-start pt-4">
                            <div>
                                <GeneralIcon IconElement={data.IconElementTrue} />
                            </div>
                            <div className="px-4">
                                <GeneralIcon IconElement={data.IconElementFalse} />
                            </div>
                        </div>
                    </div>
                </div>
            )}
        </div>
    );
};

export default ContentGuide;
