import React from "react"

export const Footer = () => {
    return (
        <footer className="w-full h-24 border-t-2 border-solid border-gray-400 flex justify-center items-center">
            <a 
                className="flex justify-center items-center flex-grow"
                href="https://pahamify.com"
                target="_blank"
                rel="noopener noreferrer"
            >
                Powered by{" Pahamify "}
                <img src="/vercel.svg" alt="Vercel Logo" className="h-4 ml-2" />
            </a>
        </footer>
    )
}