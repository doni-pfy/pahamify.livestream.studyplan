import React, { useState } from 'react';
import VideoPlayer from 'src/components/elements/VideoPlayer';

type ResultVideoProps = {
    chapter: string,
    videoUrl: string,
}

export class ResultVideo extends React.Component<ResultVideoProps, {}> {
    private videRef: any;
    private videoPlayerFuncProps = {
        onRefreshVideo: this.handleRefreshVideo.bind(this),
        onResolutionSelected: this.handleResolutionSelected.bind(this)
    }

    constructor(props: ResultVideoProps) {
        super(props);
        this.state = {
            videoUrl: this.props.videoUrl,
            ref: (ref: any) => {this.videRef = ref},
            refresh: 0,
        };
    }

    componentDidUpdate(prevProps: any) {
        if (this.props.videoUrl !== prevProps.videoUrl) {
            this.setState({
                videoUrl: this.props.videoUrl
            });
        }
    }

    handleRefreshVideo() {
        this.setState({
            refresh: Math.random()
        });
    }

    handleResolutionSelected(data: any) {
        var newUrl = this.props.videoUrl.replace(".mp4", `-${data}.mp4`);
        this.setState({
            videoUrl: newUrl
        });
    }

    render() {
        return (
            <>
                <VideoPlayer 
                    {...this.state}
                    {...this.videoPlayerFuncProps}
                />
    
                <div className="mx-4 pt-6 pb-6 border-b-2 border-base-secondary border-opacity-10">
                    <div className="font-bold text-text-primary">
                        { this.props.chapter }
                    </div>
                </div>
            </>
        );
    }
    
}