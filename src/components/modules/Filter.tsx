import React, { useEffect, useState } from "react";
import FilterIcon from "public/icon/filter.svg";
import LabelFilter from "../elements/LabelFilter";
import Slider from "react-slick";
import { Slick } from "src/constants/slick";
import { DataFilterType, FilterType } from "src/types/resultFilterType";
import FilterModal from "./FilterModal";
import { FilterLabel, FilterTitle } from "src/constants/filter";

import "slick-carousel/slick/slick-theme.css";
import "slick-carousel/slick/slick.css";
interface FilterProps {
  listBab: FilterType[];
  listKelas: FilterType[];
  onchange: (filterBab: Array<number>, filterKelas: Array<number>) => void;
}

const Filter = (props: FilterProps) => {
  const { listBab, listKelas, onchange } = props;
  const [filterBab, setFilterBab] = useState<Array<number>>([]);
  const [filterKelas, setFilterKelas] = useState<Array<number>>([]);
  const [showFilter, setShowFilter] = useState<boolean>(false);
  const [dataFilter, setDataFilter] = useState<DataFilterType[]>([]);

  useEffect(() => {
    setDataFilter([
      {
        title: FilterTitle.Bab,
        label: FilterLabel.Bab,
        data: listBab,
        filtered: filterBab,
      },
      {
        title: FilterTitle.Kelas,
        label: FilterLabel.Kelas,
        data: listKelas,
        filtered: filterKelas,
      }
    ])
  }, [listBab, listKelas, filterBab, filterKelas])

  const handleChangeFitler = (value: number, category: string) => {
    const newFilterBab = [...filterBab];
    const newFilterKelas = [...filterKelas];
    if (category === FilterLabel.Bab) {
      if (filterBab.indexOf(value) === -1) {
        newFilterBab.push(value);
      } else {
        newFilterBab.splice(filterBab.indexOf(value), 1)
      }
      setFilterBab(newFilterBab);
    } else if (category === FilterLabel.Kelas) {
      if (filterKelas.indexOf(value) === -1) {
        newFilterKelas.push(value);
      } else {
        newFilterKelas.splice(filterKelas.indexOf(value), 1)
      }
      setFilterKelas(newFilterKelas)
    }

    onchange(newFilterBab, newFilterKelas);
  };

  const handleSubmitFilter = (data: any) => {
    setFilterBab(data[FilterLabel.Bab]);
    setFilterKelas(data[FilterLabel.Kelas]);

    onchange(data[FilterLabel.Bab], data[FilterLabel.Kelas]);
    setShowFilter(false);
  }

  const renderFilterBab = () => {
    return listBab.map((item, idx) => (
      <React.Fragment key={item.id}>
        <LabelFilter
          value={item.id}
          text={item.text}
          activeProps={filterBab.indexOf(item.id) === -1 ? false : true}
          onchange={(value) =>
            handleChangeFitler(value, FilterLabel.Bab)
          }
        />
      </React.Fragment>
    ));
  };
  const renderFilterKelas = () => {
    return listKelas.map((item, idx) => (
      <React.Fragment key={item.id}>
        <LabelFilter
          value={item.id}
          text={item.text}
          activeProps={filterKelas.indexOf(item.id) === -1 ? false : true}
          onchange={(value) =>
            handleChangeFitler(value, FilterLabel.Kelas)
          }
        />
      </React.Fragment>
    ));
  };
  
  return (
    <>
      <div className="flex items-center mb-4">
        <div
          className="bg-white px-2 py-2.5 rounded-lg cursor-pointer mr-4"
          style={{ boxShadow: "0px 5px 20px rgba(32, 93, 183, 0.051)" }}
          onClick={() => setShowFilter(true)}
        >
          <FilterIcon />
        </div>

        <div className="overflow-x-hidden -mr-4" style={{transform: 'translateY(4px)'}}>
          <Slider {...Slick.Settings}>
            {renderFilterKelas()}
            {renderFilterBab()}
          </Slider>
        </div>
      </div>

      {showFilter && <FilterModal onClose={() => setShowFilter(false)} onSubmit={handleSubmitFilter} data={dataFilter}/>}

    </>
  );
};

export default Filter;
