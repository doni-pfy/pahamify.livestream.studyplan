import React from 'react';
import ImageCircle from '../elements/ImageCircle';

interface CardTestimoniProps {
    className?: string;
    imageUrl?: string;
    name: string;
    kelas: string;
    testi: string;
  }

const CardTestimoni = (props: CardTestimoniProps) => {
    const { className = "", imageUrl, name, kelas, testi } = props
    return (
        <div className={`p-4 bg-white rounded-lg ls-0.025 ${className}`} style={{boxShadow: '0px 5px 20px rgba(59, 115, 197, 0.05)'}}>
            <div className="flex mb-2">
                <div className="mr-4 flex-shrink-0">
                    <ImageCircle url={imageUrl} variant="medium"/>
                </div>
                <div className="flex-grow">
                    <h3 className="font-bold text-text-primary mb-1">{name}</h3>
                    <div className="text-xs font-bold text-text-primary text-opacity-70">{kelas}</div>
                </div>
            </div>
            <div className="text-sm text-text-primary text-opacity-70 leading-5">
                <p>{testi}</p>
            </div>
        </div>
    );
};

export default CardTestimoni;