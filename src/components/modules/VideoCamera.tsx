import React, { useEffect, useRef } from "react";
import { Camera, Canvas, Context } from "src/constants/camera";
import { MipiSubjectId } from "src/constants/windowStorage";

interface VideoCameraProps {
  captureImage: Boolean;
  setCapture: (capture: Boolean) => void;
  onCapture: (dataUri: string) => void;
  customStyle?: Object;
  className?: string;
  flip: Boolean;
}

const VideoCamera = (props: VideoCameraProps) => {
  const {
    captureImage,
    setCapture,
    onCapture,
    customStyle = {},
    className = "",
    flip,
  } = props;
  const videoPlayer = useRef<HTMLVideoElement>(null);

  useEffect(() => {
    if (!window.sessionStorage.getItem(MipiSubjectId)) return;
    let stream: any;
    let constraint = {
      audio: false,
      video: { facingMode: !flip ? Camera.BACK : Camera.FRONT },
    };
    if (videoPlayer.current) {
      videoPlayer.current.pause();
      videoPlayer.current.srcObject = null;
    }

    initCamera(constraint).then((res) => {
      stream = res;
    });

    return () => {
      if (!stream) return;
      // stop camera if leave component
      stream.getTracks().forEach((track: any) => {
        track.stop();
      });
    };
  }, [flip]);

  useEffect(() => {
    if (!captureImage) return;
    capture();
    setCapture(false);
  }, [captureImage]);

  const initCamera = async (constraint: any) => {
    let stream;
    const cameras = await navigator?.mediaDevices?.enumerateDevices();
    if (!cameras) return;

    try {
      stream = await navigator?.mediaDevices?.getUserMedia(constraint);
      if (videoPlayer.current) {
        videoPlayer.current.srcObject = stream;
        videoPlayer.current.play();
      }
    } catch (error) {
      console.log(error);
      alert(error);
    }

    return stream;
  };

  const capture = () => {
    if (videoPlayer.current) {
      const canvas: any = document.createElement(Canvas);
      canvas.width = videoPlayer.current.clientWidth;
      canvas.height = videoPlayer.current.clientHeight;
      const contex = canvas.getContext(Context["2D"]);
      const cover = fit(false);
      const { offsetX, offsetY, width, height } = cover(
        videoPlayer.current.clientWidth,
        videoPlayer.current.clientHeight,
        videoPlayer.current.videoWidth,
        videoPlayer.current.videoHeight
      );

      contex &&
        contex.drawImage(videoPlayer.current, offsetX, offsetY, width, height);

      onCapture(canvas.toDataURL());
    }
  };

  const fit = (contains: Boolean) => {
    return (
      parentWidth: number,
      parentHeight: number,
      childWidth: number,
      childHeight: number,
      scale = 1,
      offsetX = 0.5,
      offsetY = 0.5
    ) => {
      const childRatio = childWidth / childHeight;
      const parentRatio = parentWidth / parentHeight;
      let width = parentWidth * scale;
      let height = parentHeight * scale;

      if (contains ? childRatio > parentRatio : childRatio < parentRatio) {
        height = width / childRatio;
      } else {
        width = height * childRatio;
      }

      return {
        width,
        height,
        offsetX: (parentWidth - width) * offsetX,
        offsetY: (parentHeight - height) * offsetY,
      };
    };
  };

  return (
    <div className={`w-full h-full ${className}`} style={customStyle}>
      <video
        ref={videoPlayer}
        playsInline={true}
        className="w-full h-full object-cover"
      ></video>
    </div>
  );
};

export default VideoCamera;
