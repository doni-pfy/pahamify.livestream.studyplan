import React, { useState } from "react";
import { useRouter } from "next/router";
import InfoIcon from 'public/info.svg';
import { SectionHeader } from "../elements/SectionHeader";
import { SubjectDropdown } from "../elements/SubjectDropdown";
import { TextWithIcon } from "../elements/TextWithIcon";
import { FormattedMessage } from "react-intl";
import { MipiChances, MipiSubjectId } from "src/constants/windowStorage";
import { useUser } from "src/context/user.context";
import { GeneralButton } from "../elements/GeneralButton";
import Camera from "public/icon/camera.svg";
import LoginModal from "./LoginModal";

interface CardTanyaMipiProps {
    onClick?: () => void,
    routerPath?: string,
    className: string;
}

export const CardTanyaMipi = (props: CardTanyaMipiProps) => {
    const { onClick, routerPath, className } = props;
    const [modalKesempatanOpen, setModalKesempatanOpen] = React.useState<Boolean>(false);
    const [subjectId, setSubjectId] = useState<number>(0);
    const [errorSubject, setErrorSubject] = useState<boolean>(false);
    const router = useRouter();
    const {state: {user}} = useUser()

    const handleSubmit = () => {
      if (!subjectId) {
        setErrorSubject(true);
        return;
      }
      setErrorSubject(false);
      const CHANCES = window.localStorage.getItem(MipiChances) ? Number(window.localStorage.getItem(MipiChances)):3;

      if (CHANCES <= 0 && !user.isLoggedIn) {
        setModalKesempatanOpen(true);
        return;
      }
      
      window.sessionStorage.setItem(MipiSubjectId, subjectId.toString());
      router.push('/tanya-mipi');
    }

    const handleSelectSubject = (subjectId: number) => {
      setSubjectId(subjectId)
    }

    return (
        <div className={`${className} p-4 flex flex-col items-center bg-white rounded-lg`} style={{boxShadow: '0px 5px 20px rgba(59, 115, 197, 0.05)'}}>
            <SectionHeader titleId='card.title.ask.mipi' descriptionId='card.title.ask.mipi.description'/>
            <SubjectDropdown onSelected={handleSelectSubject}/>
            {
              errorSubject && (
                <FormattedMessage id="subject.dropdown.error">
                  {(message) => <p className="text-red text-xs italic self-start mt-2">{message}</p>}
                </FormattedMessage>
              )
            }
            <GeneralButton className="bg-base-primary rounded-full text-white w-full mt-4" textId='button.text.upload.photo' onClick={handleSubmit} IconElement={Camera}/>
            <TextWithIcon onClick={() => onClick} routerPath={routerPath} className="mt-4 text-base-primary" textId='text.how.to.use.feature' IconElement={InfoIcon}/>
            { modalKesempatanOpen ? ( 
                <LoginModal
                  title="card.title.kesempatan.habis"
                  desc="card.text.kesempatan.habis"
                  separatorText="separator.login"
                  onClose={() => setModalKesempatanOpen(false)}
                />
            ) : null
            }
        </div>
    )
}