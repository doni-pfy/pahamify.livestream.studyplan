import React, { useEffect, useState } from 'react';
import { useRouter } from 'next/router'

import styles from "src/styles/Header.module.scss";
import PahamifyLogo from "public/PahamifyLogo.svg";
import BackArrow from "public/icon/icon-arrow-back.svg";

import { Path } from "src/constants/path";
import { HeadMeta } from './HeadMeta';
import { useUser } from 'src/context/user.context';
import HeaderMain from '../elements/HeaderMain';
import HeaderContent from '../elements/HeaderContent';

interface PathInfo {
    url?: string,
    titleId?: string,
}

const Header = () => {
    const router = useRouter();
    const { state: { user } } = useUser();

    const [ContentPath, setContentPath] = useState(Path.ContentPath);
    const [currentPath, setcurrentPath] = useState<String>('');
    const [currentPathInfo, setCurrentPathInfo] = useState<PathInfo>({});

    useEffect(() => {
        checkCurrentPath();
    });

    const checkCurrentPath = () => {
        let pathInfo = ContentPath.find(o => o.url === router.pathname);
        if (pathInfo) setCurrentPathInfo(pathInfo);
        let path = pathInfo ? 'content' : 'main';
        setcurrentPath(`${path}`);
    };

    const logo = <PahamifyLogo className={`w-16 ${styles["icon-logo-white"]}`} />;
    const iconLeft = <BackArrow className={`${styles["icon-logo-white"]}`} />;
    return (
        <>
        <HeadMeta title={currentPathInfo.titleId} />
        <nav className="container fixed flex-wrap bg-base-primary w-full z-30">
            {currentPath === 'content' ? (
                <HeaderContent iconLeft={iconLeft} onClickLeft={() => router.back()} title={currentPathInfo.titleId}/>
            ) : (
                <HeaderMain logo={logo} isLogin={user.isLoggedIn}/>
            )}
        </nav>
        </>
    );
};

export default Header;