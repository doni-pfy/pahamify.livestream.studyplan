import Head from "next/head"
import React from "react"
import { useIntl } from "react-intl"

interface HeadMetaProps {
    title?: string;
}

export const HeadMeta = (props: HeadMetaProps) => {
    const { title } = props;
    const intl = useIntl();
    const titleMeta = title ? title : 'home.page.heading.title';
    const renderTitle = !!intl.messages[`${titleMeta}`] ? intl.formatMessage({ id: `${titleMeta}` }) : titleMeta;
    return (
        <Head>
            <title>
                { renderTitle + ' - ' + intl.formatMessage({ id: `card.title.ask.mipi` }) }
            </title>
            <link rel="icon" href="/favicon.ico" />
        </Head>
    )
}