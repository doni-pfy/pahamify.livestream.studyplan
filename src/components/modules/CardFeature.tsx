import React from "react";
import ImageCircle from "../elements/ImageCircle";

interface CardFeatureProps {
  className?: string;
  ImageFeature?: any;
  text: string;
  title: string;
}

const CardFeature = (props: CardFeatureProps) => {
  const { className = "", ImageFeature, text, title } = props;
  return (
    <div
      className={`p-4 flex bg-white rounded-lg mb-4 ${className}`}
      style={{ boxShadow: "0px 5px 20px rgba(59, 115, 197, 0.05)" }}
    >
      <div className="mr-4 flex-shrink-0">
        <ImageFeature />
      </div>
      <div className="flex-grow">
        <h5 className="text-text-primary text-opacity-70 leading-5 text-sm ls-0.025 font-bold">{title}</h5>
        <h4 className="text-text-primary text-opacity-70 leading-5 text-sm ls-0.025">
          {text}
        </h4>
      </div>
    </div>
  );
};

export default CardFeature;
