import SchoolIcon from "public/icon/school_icon.svg";
import CameraAddIcon from "public/icon/camera-add.svg";
import React from "react";
import { studentProfile } from "src/model/profile";
import { GeneralButton } from "../elements/GeneralButton";
import ImageCircle from "../elements/ImageCircle";
import Label from "../elements/Label";
import { Path } from "src/constants/path";

interface ProfileHeadingProps {
  profilePicture: string;
  bio: string;
  profile: studentProfile;
  className?: string;
  role?: string
  onBioEdit: () => void,
}

const ProfileHeading = (props: ProfileHeadingProps) => {
  const {
    profilePicture,
    profile: { Class: studentClass, FullName: name, School: school },
    className, bio, role, onBioEdit,
  } = props;

  return (
    <div className={`${className} bg-base-primary`}>
      <div className="container mx-auto flex flex-col items-center">
        <a href={Path.TakeProfilePicture} className="flex items-end justify-end cursor-pointer">
          <ImageCircle url={profilePicture} variant="big" />
          <CameraAddIcon className="absolute"/>
        </a>
        <h1 className="mt-4 text-sm font-bold leading-5 tracking-wide text-white">{name}</h1>
        <div className="flex mt-2 justify-items-center">
          <Label
            className="mx-1"
            textValue={studentClass}
            customStyle={{ backgroundColor: 'white', color: '#6A94CC' }}
          />
          <Label
            textValue={role}
            customStyle={{ backgroundColor: 'white' }}
          />
        </div>
        <div>
          <h2 className="mt-4 text-sm text-center font-bold leading-5 tracking-wide text-white text-opacity-70">
            {bio} 
            <GeneralButton
              size='sm' className='font-bold ml-2' spacing="none" noFlex
              textColor='text-white underline' textId='button.edit.profile.bio' onClick={() => onBioEdit()}
            />
          </h2>
        </div>

        <div className="flex justify-items-center items-center mt-4">
          <SchoolIcon className="mr-1" />
          <h2 className="ml-1 text-xs text-white font-semibold">{school}</h2>
        </div>

        <GeneralButton
          size='sm' className='bg-white px-4 rounded-full mt-4'
          textColor='text-base-primary' textId='button.edit.profile' routerPath={Path.Account}
        />

      </div>
    </div>
  );
};

export default ProfileHeading;
