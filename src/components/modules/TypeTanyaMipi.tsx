import React from 'react';
import LayoutSection from 'src/components/layouts/LayoutSection';
import TanyaMipiImg from 'public/icon-type-question/type-question.svg';
import { Textarea } from 'src/components/elements/Textarea';
import { FormattedMessage } from 'react-intl';

interface TypeTanyaMipiProps {
    onChange: (value: string) => void,
    value?: string,
    valid?: boolean,
}

export const TypeTanyaMipi = (props: TypeTanyaMipiProps) => {
    const { onChange, value, valid } = props;

    const handleChange = (e: React.ChangeEvent<HTMLTextAreaElement>) => {
        onChange(e.currentTarget.value);
    }

    return (
        <div className="flex flex-col items-center mx-4">
            <TanyaMipiImg className="mt-6 mb-4"/>
            <div className="mb-6">
                <div className="mb-6">
                    <FormattedMessage id="section.title.type.question">
                        {(message) => <h2 className="text-text-primary text-center px-4 mb-1 text-lg font-bold ls-0.025">{message}</h2>}
                    </FormattedMessage>
                    <FormattedMessage id="section.text.type.question">
                        {(message) => <p className={`text-text-primary text-center px-4 text-sm ls-0.025`}>{message}</p>}
                    </FormattedMessage>
                </div>

                <Textarea placeholder="placeholder.text.type.question" onChange={handleChange} value={value} valid={valid}/>
                { !valid && 
                    <FormattedMessage id="error.text.type.question">
                        {(message) => (
                            <div className="mx-4 mt-1 text-sm italic text-red">
                                {message}
                            </div>
                        )}
                    </FormattedMessage>
                }
            </div>
        </div>
    );
}