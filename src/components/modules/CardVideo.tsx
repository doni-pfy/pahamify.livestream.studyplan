import React from "react";
import { SearchQuestion } from "src/model/searchResult";
import Play from "public/icon/icon-play.svg";
import { BASE_STORAGE } from "src/constants/url";
import { useRouter } from "next/router";
import { removeSpecialChars } from "src/utils/string";
import Link from "next/link";
import { setCookie } from "src/utils/cookie";

interface CardVideoProps {
  item: SearchQuestion;
}

const CardVideo = ({ item }: CardVideoProps) => {
  const router = useRouter();
  const metadata = item.Question.Metadata;
  let prefixUrl = item.Question.Content;
  const link = removeSpecialChars(prefixUrl.valueOf()) + '!' + item.Question.ID;

  return (
    <div className="card-search mb-8">
      <h3 className="text-sm text-text-primary text-opacity-70 ls-0.025 font-semibold mb-1">
        {metadata.Chapter && metadata.Chapter.ChapterTitle}
      </h3>
      <h4 className="text-sm text-text-primary ls-0.025 font-semibold mb-2">
        {metadata.Lesson && metadata.Lesson.LessonTitle}
      </h4>
      <Link href={`/result/solution/${link}`}>
        <div
          className="w-full relative cursor-pointer rounded-lg overflow-hidden"
          style={{ paddingTop: "57%" }}
        >
          <img
            className="absolute w-full h-full left-0 top-0 object-cover"
            src={`${BASE_STORAGE}${item.Question.ImagePath}`}
            alt=""
          />
          <div
            className="overlay absolute opacity-50 left-0 top-0 w-full h-full"
            style={{ backgroundColor: "#232022" }}
          ></div>
          <div className="absolute transform -translate-x-1/2 -translate-y-1/2 top-1/2 left-1/2">
            <Play />
          </div>
        </div>
      </Link>
    </div>
  );
};

export default CardVideo;
