import React from 'react';
import { IconModal } from '../elements/IconModal';
import LoginImage from 'public/login-image.svg';
import { useRouter } from 'next/router';
import configs from 'src/config/apiconfig';

interface LoginModalProps {
  title: string;
  desc: string;
  separatorText?: string;
  onClose: () => void;
}

const LoginModal = (props: LoginModalProps) => {
  const { title, desc, onClose, separatorText } = props;
  const router = useRouter()

  const redirectToLogin = () => {
    const loginUrl = configs?.loginUrl;
    if (!loginUrl) return;
    router.push({ pathname: loginUrl, search: 'returnUrl=' + window.location.origin });
  }

  const redirectToRegister = () => {
    const loginUrl = configs?.loginUrl;
    if (!loginUrl) return;
    router.push({ pathname: loginUrl + "/register", search: 'returnUrl=' + window.location.origin });
  }

  return (
    <IconModal
      PopupImage={LoginImage}
      sectionTitle={title}
      sectionDesc={desc}
      buttonText="button.text.login"
      secondButtonText="button.second.text.login"
      separatorText={separatorText ? separatorText:"separator.or"}
      onSubmit={redirectToLogin}
      onSecondSubmit={redirectToRegister}
      withoutCloseButton
      onClose={onClose}
    />
  );
};

export default LoginModal;