import React, { useEffect, useRef, useState } from "react";
import { DataFilterType } from "src/types/resultFilterType";
import Checkbox from "../elements/Checkbox";
import LabelFilter from "../elements/LabelFilter";

interface FilterModalSectionProps {
  item: DataFilterType;
  onchange: (filter: Array<number>) => void;
  isReset: boolean;
  setResetData: (reset: boolean) => void;
}

const FilterModalSection = (props: FilterModalSectionProps) => {
  const { item, onchange, isReset, setResetData } = props;
  const [filter, setFilter] = useState<Array<number>>(item.filtered);
  const [isCheckAll, setIsCheckAll] = useState<boolean>(false);
  const firstRender = useRef(true);

  useEffect(() => {
    if (firstRender.current) {
      firstRender.current = false;
      return;
    }

    onchange(filter);
  }, [filter]);

  useEffect(() => {
    if (!isReset) return;
    setIsCheckAll(false);
    setFilter([]);
    setResetData(false);
  }, [isReset]);

  const handleChange = (value: number) => {
    const newFilter = [...filter];
    if (filter.indexOf(value) === -1) {
      newFilter.push(value);
    } else {
      newFilter.splice(filter.indexOf(value), 1);
    }
    setFilter(newFilter);
  };

  const handleCheckAll = (checked: boolean) => {
    if (checked) {
      setFilter(item.data.map((itemCheck) => itemCheck.id));
    } else {
      setFilter([])
    }
    setIsCheckAll(checked);
  };

  return (
    <div className="pt-2 pb-3 border-b-2 border-solid border-base-primary border-opacity-10">
      <div className="flex items-center justify-between mb-4">
        <div className="font-bold text-sm text-text-primary text-opacity-70">
          {item.title}
        </div>

        <Checkbox checked={isCheckAll} handleChange={handleCheckAll} text="checkbox.select.all"/>
      </div>

      <div className="flex flex-wrap">
        {item.data.map((itemCheck) => (
          <LabelFilter
            key={itemCheck.id}
            value={itemCheck.id}
            text={itemCheck.text}
            activeProps={filter.indexOf(itemCheck.id) === -1 ? false : true}
            onchange={handleChange}
            className="mb-2"
          />
        ))}
      </div>
    </div>
  );
};

export default FilterModalSection;
