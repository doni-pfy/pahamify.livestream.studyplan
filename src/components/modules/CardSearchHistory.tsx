import Link from "next/link";
import { useRouter } from "next/router";
import React from "react";
import { useIntl } from "react-intl";
import { BASE_STORAGE } from "src/constants/url";
import { getConfidenceLabel } from "src/utils/doubtsolving.converter";
import Label from "../elements/Label";

interface CardSearchistoryProps {
  routerPath: string,
  className: string,
  course: string,
  searchDate: string,
  confidenceLabel?: {label: string, className: string} | null,
  content?: string,
  contentImagePath?: string
}

export const CardSearchHistory = (props: CardSearchistoryProps) => {
  const { routerPath, className, course, searchDate, confidenceLabel, content, contentImagePath } = props;

  return (
    <Link href={routerPath}>
      <div className={`${className} cursor-pointer p-4 flex flex-col items-center bg-white rounded-lg`} style={{ boxShadow: '0px 5px 20px rgba(59, 115, 197, 0.05)' }}>
        <div className='flex w-full justify-between pb-4 border-b-2 border-base-secondary border-opacity-10'>
          <h4 className="text-text-primary text-sm font-bold">{useIntl().formatMessage({id: course})}</h4>
          <p className="text-text-primary text-opacity-70 text-xs">{searchDate}</p>
        </div>
        <div className='flex w-full justify-between mt-4'>
          <p className="text-text-primary text-opacity-70 text-xs">{useIntl().formatMessage({id: 'label.question.and.result'})}</p>
          {confidenceLabel && (
            <Label
              textId={confidenceLabel.label}
              size="sm"
              type={confidenceLabel.className}
              className="rounded-100"
            />
          )}
          </div>
        <div
          className="bg-background rounded-lg p-2 mx-4 mt-4 w-full"
        >
          {contentImagePath ? (
            <img
              className="w-full h-full left-0 top-0 object-cover"
              src={`${BASE_STORAGE}${contentImagePath}`}
              alt=""
            />
          ) : (
            <p>{content && content}</p>
          )}
        </div>
      </div>
    </Link>
  )
}