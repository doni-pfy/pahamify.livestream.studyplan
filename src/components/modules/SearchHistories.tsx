import { useLazyQuery } from "@apollo/client";
import dayjs from "dayjs";
import React, { useEffect, useState } from "react";
import { GET_USER_SEARCH } from "src/api/profile";
import { TIME_CREATED_FORMAT } from "src/constants/time";
import { SearchItem } from "src/model/profile";
import { getConfidenceLabel, getSubjectName } from "src/utils/doubtsolving.converter";
import { buildSearchPath } from "src/utils/string";
import { GeneralButton } from "../elements/GeneralButton";
import { CardSearchHistory } from "./CardSearchHistory";

interface SearchHistoriesProps {
    className?: string;
}

export const SearchHistories = (props: SearchHistoriesProps) => {
    const { className } = props;

    const [page, setPage] = useState<number>(1);
    const [itemToDisplay, setItemToDisplay] = useState<SearchItem[]>([])
    const [isLoading, setIsLoading] = useState(false);
    const [errorMsg, setErrorMsg] = useState('');

    const itemPerPage = 2;
    const [fetchItems, { data }] = useLazyQuery(GET_USER_SEARCH, {
        onError: (e) => setErrorMsg(e.message),
        variables: { Page: page, PageSize: itemPerPage }
    })

    useEffect(() => {
        fetchItems();
        setIsLoading(true);
    }, [page]);

    useEffect(() => {
        if (!data) return;
        loadItems();
    }, [data]);

    const loadItems = () => {
        try {
            setItemToDisplay([...itemToDisplay, ...data.GetUserSearchResults.Data]);
            setErrorMsg('');
        } catch (error) {
            setErrorMsg('Error while loading data. Try again later.');
        } finally {
            setIsLoading(false);
        }
    };

    const loadMore = () => {
        setPage((page) => page + 1);
    };

    return (
        <div className={`${className}`}>
            {itemToDisplay.map((item: SearchItem, index: number) => {
                return (
                    <CardSearchHistory key={index} routerPath={buildSearchPath(item)}
                        className='mb-4'
                        course={getSubjectName(item.CourseGroupID)}
                        searchDate={dayjs(item.CreatedAt).format(TIME_CREATED_FORMAT)}
                        content={item.Content}
                        contentImagePath={item.ImagePath}
                        confidenceLabel={getConfidenceLabel(item.BestConfidence)}
                    />)
            })}
            <GeneralButton
                onClick={() => {
                    loadMore();
                }}
                className="my-4 bg-transparent border-base-primary border-solid border rounded-full"
                textColor="text-base-primary"
                textId={isLoading ? "loading.button" : "button.load.more"}
            />
        </div>
    );
}

