import React, { useState } from "react";

import { Answer } from "src/model/questionFeedback";
import { SubmitButton } from "src/components/elements/SubmitButton";
import { GeneralButton } from "src/components/elements/GeneralButton";
import { GeneralModal } from "src/components/elements/GeneralModal";
import { FeedbackQuestion } from "src/components/modules/FeedbackQuestion";


interface AnswerFeedbackProps {
    className?: string,
    onSubmit: (isRelevant: Answer, isLike: Answer) => void,
    onClose: () => void,
}

export const CardFeedback = (props: AnswerFeedbackProps) => {
    const { className, onSubmit, onClose } = props;

    const [isRelevant, setIsRelevant] = useState<Answer>(null);
    const [hasErrorRelevant, setHasErrorRelevant] = useState<boolean>(false);
    const [isLike, setIsLike] = useState<Answer>(null);
    const [hasErrorLike, setHasErrorLike] = useState<boolean>(false);

    const handleFirstQuestion = (choice: Answer) => {
      setIsRelevant(choice);
      setHasErrorRelevant(false);
    }
    const handleLike = (choice: Answer) => {
      setIsLike(choice);
      setHasErrorLike(false);
    }

    const handleSubmit = () => {
        if (isRelevant === null) {
          setHasErrorRelevant(true);
          return;
        } else if (isRelevant && isLike === null) {
          setHasErrorLike(true);
          return;
        }

        if (isRelevant) onSubmit(isRelevant, isLike);
        else onSubmit(isRelevant, null);
    }

    return (
        <GeneralModal withoutCloseButton whiteBG onClose={onClose} className={className}>
            <FeedbackQuestion onChange={handleFirstQuestion} hasError={hasErrorRelevant} questionId="text.match.question.feedback" className="pb-5 w-full"/>
            { isRelevant && 
                <FeedbackQuestion onChange={handleLike} hasError={hasErrorLike} questionId="text.understand.question.feedback" className="w-full animate-fade-in-up pt-4 pb-6 border-t-2 border-base-primary border-opacity-10"/>
            }
            <SubmitButton textId="text.send.feedback" onClick={handleSubmit}></SubmitButton>
            <GeneralButton textId="text.next.time.feedback" onClick={onClose} textColor="text-base-primary text-opacity-60" className="bg-white w-full mt-2"></GeneralButton>
        </GeneralModal>
    );
}