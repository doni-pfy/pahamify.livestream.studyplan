import React, { ReactNode } from "react";
import { FormattedMessage } from "react-intl";

interface PageHeadingProps {
  title?: string;
  description?: string;
  children?: ReactNode;
  className?: string;
  hasBackground?: boolean
}

const PageHeading = (props: PageHeadingProps) => {
  const { title, description, children, className, hasBackground=true } = props;

  return (
    <div className={`${className} bg-base-primary`}>
      <div className="container mx-auto">
        <div className="px-2">

          {title && (
            <FormattedMessage id={title}>
              {(message) => <h1 className="text-center text-lg font-bold ls-0.025 text-white">{message}</h1>}
            </FormattedMessage>
            )}
          {description && (
            <FormattedMessage id={description}>
              {(message) => <p className="text-center text-white text-sm">{message}</p>}
            </FormattedMessage>
          ) }

          {children ? children : null}
        </div>
        {hasBackground && (
          <div className="heading-bg mt-min-5" style={{height: 80}}>
          </div>)}
        
      </div>
    </div>
  );
};

export default PageHeading;
