import React from 'react';

interface InputFormProps {
    type: string,
    IconElement: any,
    placeholder: string,
    onChange: (e: React.ChangeEvent<HTMLInputElement>) => void,
    className?: string
}

export const InputForm = (props: InputFormProps) => {
    const { type, IconElement, placeholder, className, onChange } = props;

    return (
        <div className={`${className} w-full flex items-center rounded-lg bg-card-primary p-2`}>
            <IconElement />
            <input onChange={onChange} className="bg-card-primary focus:outline-none w-full ml-2 text-text-primary text-opacity-50" type={type} placeholder={placeholder}></input>
        </div>
    );
}
