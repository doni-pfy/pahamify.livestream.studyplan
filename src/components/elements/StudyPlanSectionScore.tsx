import { FormattedMessage } from 'react-intl';

interface SectionHeaderProps {
    className?: string,
    titleId: string,
    descriptionId: string,
    solidDesc?: boolean,
}

export const StudyPlanSectionScore = (props: SectionHeaderProps) => {
    const { className, titleId, descriptionId, solidDesc } = props;
    return (
        <div className={`flex flex-col bg-green-200 items-center rounded-lg ${className}`}>
            <div className="p-4 pb-2 px-5">
                <FormattedMessage id={titleId}>
                    {(message) => <h1 className="text-3xl font-bold text-center mb-2">2.1%</h1>}
                </FormattedMessage>
                <FormattedMessage id={titleId}>
                {(message) => <h2 className="text-sm text-center text-text-primary mb-2 text-opacity-70">progress</h2>}
                </FormattedMessage>
            </div>
        </div>
    );
}