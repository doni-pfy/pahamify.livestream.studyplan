import React from 'react';
import { useIntl } from 'react-intl';
import styles from 'src/styles/Textarea.module.scss';

interface TextareaProps {
    placeholder?: string,
    className?: string,
    onChange?: (e: React.ChangeEvent<HTMLTextAreaElement>) => void,
    value?: string,
    valid?: boolean,
}

export const Textarea = (props: TextareaProps) => {
    const { placeholder, className, onChange, value, valid } = props;
    const intl = useIntl();
    const textareastyle = valid ? styles.textareavalid : styles.textareainvalid

    return (
        <textarea rows={6} placeholder={intl.formatMessage({id: `${placeholder}`})} onChange={onChange} value={value}
            className={`${className} ${textareastyle}`} />
    )
}