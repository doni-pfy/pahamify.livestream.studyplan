import React, { ReactNode, useEffect, useRef, useState } from "react";
import ChevronDown from "public/icon/chevron-down.svg";
import ChevronUp from "public/icon/chevron-up.svg";

import styles from "src/styles/Accordion.module.scss";

interface AccordionProps {
  title: string;
  children: ReactNode;
  activeProps?: boolean;
}

const Accordion = (props: AccordionProps) => {
  const { title, children, activeProps = false } = props;

  const [active, setActive] = useState<boolean>(activeProps);
  const [contentHeight, setContentHeight] = useState<string>("0px");
  const contentRef = useRef<HTMLDivElement>(null);

  useEffect(() => {
    if (active && contentRef.current) {
      setContentHeight(`${contentRef.current?.offsetHeight}px`)
    } else {
      setContentHeight(`0px`)
    }
  }, [active])

  return (
    <div className="relative">
      <div
        className={`flex items-center justify-between py-3 border-b-2 border-solid cursor-pointer ${
          styles["title"]
        } ${active ? styles["title-active"] : ""}`}
        style={{ borderColor: "#EAEFF7" }}
        onClick={() => setActive(!active)}
      >
        <h5 className="text-text-primary text-opacity-70 font-bold text-sm">
          {title}
        </h5>
        <ChevronDown />
      </div>
      <div className={`pl-4 ${styles['content']}`} style={{height: contentHeight}}>
        <div className="pt-2" ref={contentRef}>{children}</div>
      </div>
    </div>
  );
};

export default Accordion;
