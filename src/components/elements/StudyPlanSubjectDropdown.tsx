import CaretDown from 'public/caret-down.svg';
import PhysicsIcon from 'public/icon-course/fisika.svg';
import ChemistryIcon from 'public/icon-course/kimia.svg';
import MathIcon from 'public/icon-course/mtk.svg';
import React, { useState } from 'react';
import { FormattedMessage } from 'react-intl';
import { Major } from 'src/constants/subject';
import styles from 'src/styles/SubjectDropdown.module.scss';
import { Text } from './Text';
import { TextWithImageIcon } from './TextWithImageIcon'

interface SubjectDropdownProps {
    onSelected: (subjectId: number) => void,
    className?: String
}

export const StudyPlanSubjectDropdown = (props: SubjectDropdownProps) => {
    const [isShowDropdown, setShowDropdown] = useState<boolean>(false);
    const [selectedItem, setSelectedItem] = useState<number>(0);
    const {onSelected} = props;

    function onDropdownClicked(textId: number) {
        setSelectedItem(textId);
        onSelected(textId);
        setShowDropdown(!isShowDropdown);
    }

    return (
        <div className="mx-4 my-4 mt-0 relative z-10">
            <div onClick={() => onDropdownClicked(0)} className="flex mt-4 justify-between items-center rounded-lg bg-card-primary py-2 px-4 cursor-pointer">
                <button className={`text-text-primary font-bold text-left`}>
                    {getButtonFill(selectedItem)}
                </button>
                <CaretDown />
            </div>
            <div id="dropdown-id" className={`${styles['dropdown']} w-full absolute mt-2 flex flex-col p-4 items-start bg-white rounded-lg ${isShowDropdown ? '' : 'hidden'}`}>
                <Text onClick={() => onDropdownClicked(Major.Saintek.Id)} className="w-full cursor-pointer" textId={Major.Saintek.Text} />
                <Text onClick={() => onDropdownClicked(Major.Soshum.Id)} className="w-full cursor-pointer mt-4" textId={Major.Soshum.Text} />
                <Text onClick={() => onDropdownClicked(Major.Campuran.Id)} className="w-full cursor-pointer mt-4" textId={Major.Campuran.Text} />
            </div>
        </div>
    );

    function getButtonFill(selectedItem: number) {
        switch (selectedItem) {
            case Major.Saintek.Id : return <TextWithImageIcon IconElement="/icon-course/search.png" textId={Major.Saintek.Text} />
            case Major.Soshum.Id: return <TextWithImageIcon IconElement="/icon-course/search.png" textId={Major.Soshum.Text} />
            case Major.Campuran.Id: return <TextWithImageIcon IconElement="/icon-course/search.png" textId={Major.Campuran.Text} />
            default: return  <TextWithImageIcon IconElement="/icon-course/search.png" textId={Major.Saintek.Text} />
        }

    }
}
