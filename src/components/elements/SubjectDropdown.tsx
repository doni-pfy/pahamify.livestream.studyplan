import CaretDown from 'public/caret-down.svg';
import PhysicsIcon from 'public/icon-course/fisika.svg';
import ChemistryIcon from 'public/icon-course/kimia.svg';
import MathIcon from 'public/icon-course/mtk.svg';
import React, { useState } from 'react';
import { FormattedMessage } from 'react-intl';
import { Subject } from 'src/constants/subject';
import styles from 'src/styles/SubjectDropdown.module.scss';
import { TextWithIcon } from './TextWithIcon';

interface SubjectDropdownProps {
    onSelected: (subjectId: number) => void,
    className?: String
}

export const SubjectDropdown = (props: SubjectDropdownProps) => {
    const [isShowDropdown, setShowDropdown] = useState<boolean>(false);
    const [selectedItem, setSelectedItem] = useState<number>(0);
    const {onSelected} = props;

    function onDropdownClicked(textId: number) {
        setSelectedItem(textId);
        onSelected(textId);
        setShowDropdown(!isShowDropdown);
    }

    return (
        <div className="w-full relative z-10">
            <div onClick={() => onDropdownClicked(0)} className="flex w-full h-full mt-4 justify-between items-center rounded-lg bg-card-primary py-2 px-4 cursor-pointer">
                <button className={`${!selectedItem ? 'italic' : ''} text-text-primary text-opacity-50 text-left`}>
                    {getButtonFill(selectedItem)}
                </button>
                <CaretDown />
            </div>
            <div id="dropdown-id" className={`${styles['dropdown']} w-full absolute mt-2 flex flex-col p-4 items-start bg-white rounded-lg ${isShowDropdown ? '' : 'hidden'}`}>
                <TextWithIcon onClick={() => onDropdownClicked(Subject.Matematika.Id)} className="w-full cursor-pointer" IconElement={MathIcon} textId={Subject.Matematika.Text} />
                <TextWithIcon onClick={() => onDropdownClicked(Subject.Fisika.Id)} className="w-full cursor-pointer mt-4" IconElement={PhysicsIcon} textId={Subject.Fisika.Text} />
                <TextWithIcon onClick={() => onDropdownClicked(Subject.Kimia.Id)} className="w-full cursor-pointer mt-4" IconElement={ChemistryIcon} textId={Subject.Kimia.Text} />
            </div>
        </div>
    );

    function getButtonFill(selectedItem: number) {
        switch (selectedItem) {
            case Subject.Matematika.Id : return <TextWithIcon IconElement={MathIcon} textId={Subject.Matematika.Text} />
            case Subject.Fisika.Id: return <TextWithIcon IconElement={PhysicsIcon} textId={Subject.Fisika.Text} />
            case Subject.Kimia.Id: return <TextWithIcon IconElement={ChemistryIcon} textId={Subject.Kimia.Text} />
            default: return <FormattedMessage id={'subject.choose'} />
        }

    }
}
