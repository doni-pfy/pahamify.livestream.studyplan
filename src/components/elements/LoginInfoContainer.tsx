import React, { useState } from 'react';
import { GeneralButton } from "src/components/elements/GeneralButton";
import NotificationButton from "src/components/elements/NotificationButton";
import { useAppState } from 'src/context/appstate.context';
import { AppStateActionType } from 'src/model/appstate';


interface LoginInfoContainerProps {
    IconElement?: any,
    onClick?: () => void,
    textId: string,
    className?: string,
    textColor?: string,
    isLogin: Boolean,
}

const LoginInfoContainer = (props: LoginInfoContainerProps) => {
    const { state: { appState }, dispatch: dispatchAppState } = useAppState();

    const { IconElement, onClick, textId, className, textColor, isLogin } = props;
    const [isNotification, setNotificationCondition] = useState(true);
    const checkNotification = (isNotification: Boolean) => {
        return setNotificationCondition(!isNotification);
    };

    return (
        <>
            <div>
                {isLogin === true ? (
                    <NotificationButton onClick={() => checkNotification(isNotification)} isNotification={isNotification} />
                ) : (
                    <GeneralButton size="sm" textId={textId} IconElement={IconElement} className={`mt-2 ${className}`} textColor={textColor}
                        onClick={() => dispatchAppState({
                            type: AppStateActionType.SetModalLogin,
                            payload: { ...appState, isModalLoginOpen: true }
                        })}
                    />
                )}
            </div>
        </>
    );
};

export default LoginInfoContainer;
