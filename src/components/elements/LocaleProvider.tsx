import React, { ReactNode } from 'react';
import { IntlProvider } from 'react-intl';
import { LOCALE_ID, MESSAGES } from 'src/constants/locale';


interface LocaleProps {
    children: ReactNode;
}

export const LocaleProvider = (props: LocaleProps) => {
    const handleError = (err: any) => {
        // change the default error to a warning
        if (err.code === "MISSING_TRANSLATION") {
            console.warn("Missing translation", err.message);
            return;
        }
        console.log("warning : ", err);
    }

    return (
        <IntlProvider locale={LOCALE_ID} messages={MESSAGES[LOCALE_ID]} onError={handleError}>
            {props.children}
        </IntlProvider>
    );
}
