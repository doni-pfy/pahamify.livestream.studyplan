import { FormattedMessage } from 'react-intl';

interface SectionHeaderProps {
    className?: string,
    titleId: string,
    descriptionId: string,
    solidDesc?: boolean,
}

export const StudyPlanSectionHeader = (props: SectionHeaderProps) => {
    const { className, titleId, descriptionId, solidDesc } = props;
    return (
        <div className={`flex flex-col flex-1 ${className}`}>
            <FormattedMessage id={titleId}>
                {(message) => <h2 className="text-text-primary font-bold leading-5 tracking-wide mb-2">Nama User</h2>}
            </FormattedMessage>

            <FormattedMessage id={titleId}>
                {(message) => <h2 className="text-base leading-5 tracking-wide text-text-primary text-opacity-70">9 hari lagi</h2>}
            </FormattedMessage>

            <FormattedMessage id={descriptionId} values={{br: <br />, note: <b>Penting:</b>}}>
                {(message) => <p className={`${solidDesc ? "" : "text-opacity-70"} text-sm font-normal leading-5 tracking-wide text-text-primary`}>Menuju SIMAK UI</p>}
            </FormattedMessage>
        </div>
    );
}