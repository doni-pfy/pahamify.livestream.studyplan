import CaretDown from 'public/caret-down.svg';
import PhysicsIcon from 'public/icon-course/fisika.svg';
import ChemistryIcon from 'public/icon-course/kimia.svg';
import MathIcon from 'public/icon-course/mtk.svg';
import React, { useState } from 'react';
import { FormattedMessage } from 'react-intl';
import { Major } from 'src/constants/subject';
import styles from 'src/styles/SubjectDropdown.module.scss';
import { Text } from './Text';
import { TextWithImageIcon } from './TextWithImageIcon'
import { StudyPlanAccordionSubtitle } from './StudyPlanAccordionSubtitle'
import { StudyPlanAccordionHeaderTitle } from './StudyPlanAccordionHeaderTitle'
import { StudyPlanAccordionListItem } from './StudyPlanAccordionListItem'
import studyPlanData from 'src/constants/StudyPlanData.json'

interface StudyPlanAccordionTitleProps {
    onSelected?: (subjectId: number) => void,
    className?: String
    majorName: string
}

export const StudyPlanAccordion = (props: StudyPlanAccordionTitleProps) => {
    const [isShowDropdown, setShowDropdown] = useState<boolean>(false);
    const [selectedItem, setSelectedItem] = useState<number>(0);
    const [isOpen, setOpen] = useState(false)
    const {onSelected, majorName} = props;

    function onDropdownClicked(textId: number) {
        setSelectedItem(textId);
        onSelected?.(textId);
        setShowDropdown(!isShowDropdown);
    }

    const getStudyPlanData = () => {
        switch(majorName) {
            case 'Saintek': 
                return studyPlanData.saintek
            case 'Soshum': 
                return studyPlanData.soshum
            case 'TPS': 
                return studyPlanData.tps
            default:
                return studyPlanData.saintek
        }
    }

    return (
        <>
        <div className="my-4 relative flex flex-row justify-end">
           <Text className="w-full text-base-primary" onClick={() => {}} text={majorName}/>
           <div onClick={() => setOpen(!isOpen)}>
           {
               isOpen ? (
                <img src="/icon-course/chevron_up.png" width="15" />
               ): 
               <img src="/icon-course/chevron_down.png" width="15" />
           }
           </div>
        </div>
            <div>
                {
                    getStudyPlanData().map((data: any) => (
                        <div key={data.courseId}>
                            <StudyPlanAccordionSubtitle courseTitle={data.courseTitle} progress={data.progress ? Math.round(data.progress * 10) / 10 : 0}/>
                            {
                                !!isOpen && (
                                    <>
                                     <StudyPlanAccordionHeaderTitle/>
                                    {
                                        data.bigChapterList.map((chapter: any) => (
                                        <StudyPlanAccordionListItem chapterData={chapter} chapterTitle={chapter.bigChapterTitle} frequency={chapter.frekuensi} progress={chapter.progress} />
                                        ))
                                    }
                                    </>
                                )
                            }
                        </div>
                    ))
                }
            </div>
        
        </>
    );
}
