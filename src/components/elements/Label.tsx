import React from 'react';
import { FormattedMessage } from 'react-intl';
import styles from 'src/styles/Label.module.scss';

interface LabelProps {
  size?: string;
  customStyle?: object;
  className?: string;
  type?: string;
  textId?: string;
  textValue?: string;
}

const Label = (props: LabelProps) => {
  const { size, customStyle, className = '', textId, textValue, type = '' } = props;
  let sizeClass = '';
  switch (size) {
    case 'sm':
      sizeClass = 'text-xs py-0.5 px-2'
      break;
    default:
      sizeClass = 'text-sm py-1 px-2'
      break;
  }

  let typeClass = '';
  switch (type) {
    case 'success':
      typeClass = 'label-success'
      break;
    case 'warning':
      typeClass = 'label-warning'
      break;
    case 'danger':
      typeClass = 'label-danger'
      break;
    default:
      typeClass = 'label-default'
      break;
  }

  return (
    <label className={`${styles[typeClass]} ${sizeClass} rounded-md ${className}`} style={customStyle}>
      {textId ? <FormattedMessage id={textId}>
        {(message) => <span>{message}</span>}
      </FormattedMessage> :
        <span>{textValue}</span>
      }
    </label>
  );
};

export default Label;