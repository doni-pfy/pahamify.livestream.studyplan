import CaretDown from 'public/caret-down.svg';
import PhysicsIcon from 'public/icon-course/fisika.svg';
import ChemistryIcon from 'public/icon-course/kimia.svg';
import MathIcon from 'public/icon-course/mtk.svg';
import React, { useState } from 'react';
import { FormattedMessage } from 'react-intl';
import { Major } from 'src/constants/subject';
import styles from 'src/styles/SubjectDropdown.module.scss';
import { Text } from './Text';
import { TextWithImageIcon } from './TextWithImageIcon'
import { useRouter } from "next/router";

interface StudyPlanAccordionSubtitleProps {
    onSelected?: (subjectId: number) => void,
    className?: String
    chapterTitle: string
    progress: Number
    frequency: number
    chapterData: any
}

export const StudyPlanDetailsAccordionListItem = (props: StudyPlanAccordionSubtitleProps) => {
    const [isShowDropdown, setShowDropdown] = useState<boolean>(false);
    const [selectedItem, setSelectedItem] = useState<number>(0);
    const [isSelected, setSelected] = useState(false)
    const {chapterData, chapterTitle, frequency, progress, onSelected} = props;
    const router = useRouter()

    function onDropdownClicked(textId: number) {
        setSelectedItem(textId);
        onSelected?.(textId);
        setShowDropdown(!isShowDropdown);
    }

    // const progress = Math.floor(Math.random() * (100 - 60 + 1)) + 60;

    return (
        <div className="relative my-4 mx-2 flex flex-row justify-items-center">
            <div className="bg-base-primary" style={{width: 10, height: 10, borderRadius: 5, marginTop: 5}}/>
            <div className="flex-1 ml-2 pb-2 relative flex flex-row justify-between" style={{borderBottom: "1px solid rgba(225,232,242,0.9)"}}>
                <Text className="w-2/3" textClassName="text-xs font-bold text-base-primary" text={chapterTitle} />
                <div className="flex flex-row justify-items-center">
                <input type="checkbox" checked={isSelected} onChange={() => {
                    setSelected(!isSelected)
                }} className="h-6 w-6 border border-gray-300 rounded-md checked:bg-blue-600 checked:border-transparent focus:outline-none"/>
                </div>
            </div>
        </div>
    );
}
