import { FormattedMessage } from 'react-intl';

interface SectionHeaderProps {
    className?: string,
    titleId: string,
    descriptionId: string,
    solidDesc?: boolean,
}

export const SectionHeader = (props: SectionHeaderProps) => {
    const { className, titleId, descriptionId, solidDesc } = props;
    return (
        <div className={`flex flex-col items-center text-center ${className}`}>
            <FormattedMessage id={titleId}>
                {(message) => <h2 className="text-base font-bold leading-5 tracking-wide text-text-primary mb-2">{message}</h2>}
            </FormattedMessage>

            <FormattedMessage id={descriptionId} values={{br: <br />, note: <b>Penting:</b>}}>
                {(message) => <p className={`${solidDesc ? "" : "text-opacity-70"} text-sm font-normal leading-5 tracking-wide text-text-primary`}>{message}</p>}
            </FormattedMessage>
        </div>
    );
}