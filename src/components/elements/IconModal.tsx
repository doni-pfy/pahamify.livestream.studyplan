import React from "react";
import { GeneralModal } from "./GeneralModal";
import { SectionHeader } from "src/components/elements/SectionHeader";
import { SubmitButton } from "src/components/elements/SubmitButton";
import { GeneralButton } from "src/components/elements/GeneralButton";

import styles from 'src/styles/GeneralModal.module.scss';
import { useIntl } from "react-intl";

interface IconModalProps {
    className?: string,

    PopupImage?: any,
    sectionTitle: string,
    sectionDesc: string,
    buttonText: string,
    secondButtonText?: string,
    closeButtonText?: string,
    separatorText?: string,

    withoutCloseButton?: boolean,
    whiteBG?: boolean,

    onSubmit: () => void,
    onSecondSubmit?: () => void,
    onClose: () => void,
}

export const IconModal = (props: IconModalProps) => {
    const { className, PopupImage, sectionTitle, sectionDesc,
        buttonText, secondButtonText, closeButtonText,
        withoutCloseButton, whiteBG, onSubmit, onSecondSubmit, onClose, separatorText }
        = props;

    return (
        <GeneralModal onClose={onClose} withoutCloseButton={withoutCloseButton} whiteBG={whiteBG} className={className}>
            { PopupImage ? <PopupImage className="mb-6" /> : null}
            <SectionHeader className="mx-2" titleId={sectionTitle} descriptionId={sectionDesc} solidDesc={true}/>
            <SubmitButton onClick={() => onSubmit()} className="mt-4" textId={buttonText} />
            {
                closeButtonText ?
                    <GeneralButton textId={closeButtonText} onClick={onClose}
                        textColor="text-base-primary text-opacity-60"
                        className="bg-white w-full mt-2"></GeneralButton>
                    : null
            }

            {(secondButtonText && onSecondSubmit) && (
                <>
                    <p className={`mt-4 w-full text-xs text-center z-10 relative ${styles.background}`}>
                        <span className="px-2 bg-white">{useIntl().formatMessage({ id: separatorText ? separatorText:"separator.or" })}</span>
                    </p>
                    <SubmitButton onClick={() => onSecondSubmit()} className="mt-4" textId={secondButtonText} />
                </>
            )}
        </GeneralModal>
    );
}