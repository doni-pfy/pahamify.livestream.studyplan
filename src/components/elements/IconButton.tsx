import React from 'react';

interface IconButtonProps {
    className?: string,
    onClick: () => void,
    IconElement: any,
}

export const IconButton = (props: IconButtonProps) => {
    const { className, onClick, IconElement } = props;

    return (
        <a onClick={onClick} className={`${className}`}>
            <IconElement />
        </a>
    )
}