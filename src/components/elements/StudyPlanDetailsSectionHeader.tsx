import { FormattedMessage } from 'react-intl';
import { Text } from './Text';

interface SectionHeaderProps {
    className?: string,
    title: string,
    descriptionId: string,
    solidDesc?: boolean,
    progress?: Number
}

export const StudyPlanDetailsSectionHeader = (props: SectionHeaderProps) => {
    const { className, title, descriptionId, solidDesc, progress } = props;
    return (
        <div className={`flex flex-col flex-1 ${className}`}>
            <Text textClassName="text-base font-bold text-center mb-2" text={title} />
            <div className="mt-4 flex flex-row justify-items-end items-center">
                <Text className="flex-1 opacity-50" textClassName="text-xs text-opacity-50 text-center" text="Progress Belajarmu:"></Text>
                <div>
                    <Text className="bg-orange-200 py-2 pr-2 pl-1 text-white rounded-3xl" onClick={() => {}} text={`${String(progress)}%`}/>
                </div>
            </div>
        </div>
    );
}