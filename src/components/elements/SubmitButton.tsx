import { FormattedMessage } from 'react-intl';

interface SubmitButtonProps {
    textId: string,
    onClick: () => void,
    className?: String
}

export const SubmitButton = (props: SubmitButtonProps) => {
    const { textId, onClick, className } = props;
    return (
        <a onClick={onClick} className={`${className} cursor-pointer flex flex-col py-1 w-full items-center bg-base-primary rounded-3xl`}>
            <FormattedMessage id={textId}>
                {(message) => <span className="text-sm text-white font-bold leading-5 tracking-wide">{message}</span>}
            </FormattedMessage>
        </a>
    );
}