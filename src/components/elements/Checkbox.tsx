import React, { useEffect, useRef, useState } from "react";
import Check from "public/icon/check.svg";
import styles from "src/styles/Checkbox.module.scss";
import { FormattedMessage } from "react-intl";

interface CheckboxProps {
  text: string;
  checked?: boolean;
  handleChange: (checked: boolean) => void;
}

const Checkbox = (props: CheckboxProps) => {
  const { checked = false, handleChange, text } = props;

  return (
    <div
      className={`relative inline-flex items-center cursor-pointer ${styles["wrapper"]}`}
      onClick={() => handleChange(!checked)}
    >
      <div
        className={`mr-1 border-base-primary flex-shrink-0 ${styles["icon"]} ${checked ? 'bg-base-primary':''}`}
      >
        <Check />
      </div>
      <FormattedMessage id={text}>
        {(message) => <div className="label font-bold text-xs text-base-primary">{message}</div>}
      </FormattedMessage>
    </div>
  );
};

export default Checkbox;
