import CaretDown from 'public/caret-down.svg';
import PhysicsIcon from 'public/icon-course/fisika.svg';
import ChemistryIcon from 'public/icon-course/kimia.svg';
import MathIcon from 'public/icon-course/mtk.svg';
import React, { useState } from 'react';
import { FormattedMessage } from 'react-intl';
import { Major } from 'src/constants/subject';
import styles from 'src/styles/SubjectDropdown.module.scss';
import { Text } from './Text';
import { TextWithImageIcon } from './TextWithImageIcon'

interface StudyPlanAccordionSubtitleProps {
    onSelected?: (subjectId: number) => void,
    className?: String
    courseTitle: string
    progress: Number
}

export const StudyPlanAccordionSubtitle = (props: StudyPlanAccordionSubtitleProps) => {
    const [isShowDropdown, setShowDropdown] = useState<boolean>(false);
    const [selectedItem, setSelectedItem] = useState<number>(0);
    const {courseTitle, progress, onSelected} = props;

    function onDropdownClicked(textId: number) {
        setSelectedItem(textId);
        onSelected?.(textId);
        setShowDropdown(!isShowDropdown);
    }

    return (
        <div className="my-4 ml-2 relative flex flex-row justify-end">
            <MathIcon/>
            <Text className="w-full text-text-primary" onClick={() => {}} text={courseTitle}/>
            <div>
                <Text className="bg-orange-200 py-2 pr-2 pl-1 text-white rounded-3xl" onClick={() => {}} text={`${String(progress)}%`}/>
            </div>
        </div>
    );
}
