import React, { useState } from 'react';
import { FormattedMessage } from 'react-intl';
import { Major } from 'src/constants/subject';
import styles from 'src/styles/SubjectDropdown.module.scss';
import { Text } from './Text';
import { TextWithImageIcon } from './TextWithImageIcon'

interface StudyPlanAccordionSubtitleProps {
    onSelected?: (subjectId: number) => void,
    className?: String
    courseTitle: string
    difficulty: Number
}

export const StudyPlanDetailsAccordionSubtitle = (props: StudyPlanAccordionSubtitleProps) => {
    const [isShowDropdown, setShowDropdown] = useState<boolean>(false);
    const [selectedItem, setSelectedItem] = useState<number>(0);
    const {courseTitle, difficulty, onSelected} = props;

    function onDropdownClicked(textId: number) {
        setSelectedItem(textId);
        onSelected?.(textId);
        setShowDropdown(!isShowDropdown);
    }

    return (
        <div className="my-4 ml-2 relative flex flex-row justify-end">
            <img src="/icon-course/chevron_down.png" width="15" />
            <Text className="w-full text-text-primary" onClick={() => {}} text={courseTitle}/>
            <div className="flex items-center"> 
                <div className={`${difficulty === 0 || difficulty > 50 ? "bg-green" : "bg-red"} ml-1`} style={{width: 10, height: 10, borderRadius: 5}}/>
                <Text textClassName="text-xs" onClick={() => {}} text={`${difficulty ? String(difficulty) : 50}`} />
            </div>
        </div>
    );
}
