import React from 'react';

interface ImageCircleProps {
    url: string | undefined;
    alt?: string;
    variant?: "small" | "big" | "medium";
    className?: string;
}

const ImageCircle = ({ url, alt, variant = "small", className }: ImageCircleProps) => {
    return (
        <div className={`${className} ${variant === "small" ? 'w-6 h-6': variant==="medium" ? 'w-12 h-12':'w-20 h-20' } relative m-auto rounded-full bg-base-primary bg-opacity-30`}>
            {url ? ( <div/>
            ) : null}
        </div>
    );
};

export default ImageCircle;