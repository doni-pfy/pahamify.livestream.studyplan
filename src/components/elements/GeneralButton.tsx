import Link from 'next/link';
import { FormattedMessage } from 'react-intl';

import { TextWithIcon } from "../elements/TextWithIcon";

interface GeneralButtonProps {
    IconElement?: any,
    textId: string,
    onClick?: () => void,
    className?: String,
    textColor?: String,
    size?: String,
    spacing?: String,
    routerPath?: string
    noFlex?: Boolean
}

export const GeneralButton = (props: GeneralButtonProps) => {
    const { IconElement, textId, onClick, className, textColor, size, spacing, routerPath, noFlex } = props;
    var sizeStyle = `text-base`;
    let spacingStyle = ''
    switch (size) {
        case "sm":
            sizeStyle = `text-sm`;
            spacingStyle = 'py-1 px-3';
            break;
        case "md":
        default:
            sizeStyle = `text-base`;
            spacingStyle = 'py-2 px-4'
            break;
    }

    switch (spacing) {
        case "none":
            spacingStyle = '';
            break;
    }

    const flex = noFlex ? '' : 'items-center flex flex-col';

    return (
        <a href={routerPath} onClick={onClick} className={`${className ? className : 'bg-base-primary rounded-full text-white'} cursor-pointer font-bold ${flex} ${sizeStyle} ${spacingStyle}`}>
            {IconElement ? (
                <TextWithIcon textId={textId} IconElement={IconElement}/>
            ): (
                <FormattedMessage id={textId}>
                    {(message) => <span className={`${textColor ? textColor : 'text-white'} font-bold leading-5 tracking-wide`}>{message}</span>}
                </FormattedMessage>
            )}
        </a>
    );
}