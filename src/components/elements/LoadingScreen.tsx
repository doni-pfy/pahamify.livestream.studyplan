import React from "react";
import { FormattedMessage } from "react-intl";

interface LoadingProps {
  text?: string;
}

const LoadingScreen = (props: LoadingProps) => {
  const { text = "" } = props;
  return (
    <div
      className="w-full h-screen absolute top-0 left-0 flex flex-col items-center justify-center"
      style={{ backgroundColor: "rgba(225,232,242,0.9)", zIndex: 99 }}
    >
      <img src="/loading.svg" alt="" />
      <FormattedMessage id={text}>
        {(message) => (
          <span className="text-text-primary text-opacity-70">{message}</span>
        )}
      </FormattedMessage>
    </div>
  );
};

export default LoadingScreen;
