import React from "react";

interface LabelFilterProps {
  activeProps: boolean;
  text: String;
  value: number;
  onchange: (data: any) => void;
  className?: string;
}

const LabelFilter = (props: LabelFilterProps) => {
  const { activeProps, text, onchange, value, className='' } = props;
  
  const activeClass = activeProps
  ? "bg-base-primary bg-opacity-70 text-white"
  : "text-text-primary text-opacity-70 bg-base-primary bg-opacity-10";

  const handleClick = () => {
    onchange(value)
  }

  return (
    <label
      className={`text-sm py-1 px-2 rounded-full mr-2 font-bold cursor-pointer inline-block whitespace-nowrap overflow-x-hidden overflow-ellipsis ${activeClass} ${className}`}
      style={{maxWidth: '250px'}}
      onClick={handleClick}
    >
      {text}
    </label>
  );
};

export default LabelFilter;
