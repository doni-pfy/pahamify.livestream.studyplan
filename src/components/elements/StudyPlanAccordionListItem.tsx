import CaretDown from 'public/caret-down.svg';
import PhysicsIcon from 'public/icon-course/fisika.svg';
import ChemistryIcon from 'public/icon-course/kimia.svg';
import MathIcon from 'public/icon-course/mtk.svg';
import React, { useState } from 'react';
import { FormattedMessage } from 'react-intl';
import { Major } from 'src/constants/subject';
import styles from 'src/styles/SubjectDropdown.module.scss';
import { Text } from './Text';
import { TextWithImageIcon } from './TextWithImageIcon'
import { useRouter } from "next/router";

interface StudyPlanAccordionSubtitleProps {
    onSelected?: (subjectId: number) => void,
    className?: String
    chapterTitle: string
    progress: Number
    frequency: string
    chapterData: any
}

export const StudyPlanAccordionListItem = (props: StudyPlanAccordionSubtitleProps) => {
    const [isShowDropdown, setShowDropdown] = useState<boolean>(false);
    const [selectedItem, setSelectedItem] = useState<number>(0);
    const {chapterData, chapterTitle, frequency, progress, onSelected} = props;
    const router = useRouter()

    function onDropdownClicked(textId: number) {
        setSelectedItem(textId);
        onSelected?.(textId);
        setShowDropdown(!isShowDropdown);
    }

    // const progress = Math.floor(Math.random() * (100 - 60 + 1)) + 60;

    return (
        <div className="relative my-4 mx-2 flex flex-row justify-items-center">
            <div className="bg-base-primary" style={{width: 10, height: 10, borderRadius: 5}}/>
            <div className="flex-1 ml-2 pb-2 relative flex flex-row" style={{borderBottom: "1px solid rgba(225,232,242,0.9)"}}>
                <Text className="w-1/2" textClassName="text-xs font-bold text-base-primary" onClick={() => {
                    router.push(`/studyplan/details?chapter=${JSON.stringify(chapterData)}`)
                }} text={chapterTitle} />
                <div className="w-1/3 flex flex-row justify-items-center">
                    <div className="flex items-center">
                        <div className={`${frequency !== 'Sedikit' ? 'bg-green' : 'bg-red'} ml-1`} style={{width: 10, height: 10, borderRadius: 5}}/>
                    </div>
                    <Text textClassName="text-xs" onClick={() => {}} text={frequency} />
                </div>
                <div className="w-1/3 flex flex-row justify-items-center">
                    <div className="flex items-center"> 
                        <div className={`${progress > 50 ? "bg-green" : "bg-red"} ml-1`} style={{width: 10, height: 10, borderRadius: 5}}/>
                    </div>
                    <Text textClassName="text-xs" onClick={() => {}} text={`${progress ? String(progress) : 0}%`} />
                </div>
            </div>
        </div>
    );
}
