import React from 'react';

import styles from "src/styles/NotificationButton.module.scss";
import IconNotification from "public/icon/icon-notification.svg";

interface NotificationButtonProps {
    onClick: () => void,
    isNotification: Boolean,
}

const NotificationButton = (props: NotificationButtonProps) => {
    const { onClick, isNotification } = props;

    return (
        <div onClick={() => onClick()} className="py-2 px-4">
            <span className={isNotification === true ? styles['icon-notification'] : ''}></span>
            <IconNotification className={`${styles['icon-color']} mx-auto`} />
        </div>
    );
};

export default NotificationButton;
