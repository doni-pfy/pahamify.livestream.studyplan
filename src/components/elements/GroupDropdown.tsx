import CaretDown from 'public/caret-down.svg';
import PhysicsIcon from 'public/icon-course/fisika.svg';
import ChemistryIcon from 'public/icon-course/kimia.svg';
import MathIcon from 'public/icon-course/mtk.svg';
import React, { useState } from 'react';
import { FormattedMessage } from 'react-intl';
import { Subject } from 'src/constants/subject';
import styles from 'src/styles/SubjectDropdown.module.scss';
import { TextWithIcon } from './TextWithIcon';

interface SubjectDropdownProps {
    onSelected: (subjectId: number) => void,
    className?: String
}

export const GroupDropDown = (props: SubjectDropdownProps) => {
    const [isShowDropdown, setShowDropdown] = useState<boolean>(false);
    const [selectedItem, setSelectedItem] = useState<number>(0);
    const { onSelected } = props;

    function onDropdownClicked(textId: number) {
        setSelectedItem(textId);
        onSelected(textId);
        setShowDropdown(!isShowDropdown);
    }

    return (
        <div className="relative z-10">
            <div onClick={() => setShowDropdown(!isShowDropdown)} className="flex w-full h-full justify-between items-center rounded-lg bg-card-primary py-2 px-4 cursor-pointer">
                <button className={`${!selectedItem ? 'italic' : ''} text-text-primary text-opacity-50 text-left mr-2`}>
                    {getButtonFill(selectedItem)}
                </button>
                <CaretDown />
            </div>
            <div id="dropdown-id" className={`${styles['dropdown']} w-full absolute mt-2 flex flex-col p-4 items-start bg-white rounded-lg ${isShowDropdown ? '' : 'hidden'}`}>
                    <div onClick={() => onDropdownClicked(0)} className="text-sm ml-2 font-bold leading-4 tracking-wide cursor-pointer">
                        SAINTEK
                    </div>
                    <div onClick={() => onDropdownClicked(1)} className="text-sm mt-4 ml-2 font-bold leading-4 tracking-wide cursor-pointer">
                        SOSHUM
                    </div>
                {/* <TextWithIcon onClick={() => onDropdownClicked(Subject.Matematika.Id)} className="w-full cursor-pointer" IconElement={MathIcon} textId={Subject.Matematika.Text} />
                <TextWithIcon onClick={() => onDropdownClicked(Subject.Fisika.Id)} className="w-full cursor-pointer mt-4" IconElement={PhysicsIcon} textId={Subject.Fisika.Text} />
                <TextWithIcon onClick={() => onDropdownClicked(Subject.Kimia.Id)} className="w-full cursor-pointer mt-4" IconElement={ChemistryIcon} textId={Subject.Kimia.Text} /> */}
            </div>
        </div>
    );

    function getButtonFill(selectedItem: number) {
        switch (selectedItem) {
            case 0: return <p>SAINTEK</p>
            case 1: return <p>SOSHUM</p>
        }

    }
}
