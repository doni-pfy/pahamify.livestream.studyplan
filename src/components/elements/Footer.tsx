import Link from "next/link";
import React, { useEffect, useState } from "react";
import { FormattedMessage } from "react-intl";

import styles from 'src/styles/Footer.module.scss';

const Footer = () => {
  const [footerYear, setFooterYear] = useState<string>("2020");

  useEffect(() => {
    checkFooterYear();
  }, []);

  const checkFooterYear = () => {
    return setFooterYear(`${new Date().getFullYear()}`);
  };

  return (
    <footer className="mt-8 ">

      <div className="p-4 mb-16 bg-text-primary opacity-70">
        <FormattedMessage id="footer.text">
          {(message) => (
            <span className="text-base text-white font-bold leading-5 tracking-wide">
              © {footerYear} {message}
            </span>
          )}
        </FormattedMessage>
      </div>
    </footer>
  );
};

export default Footer;
