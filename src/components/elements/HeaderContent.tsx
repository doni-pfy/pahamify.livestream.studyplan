import React, { ReactNode } from 'react';
import { FormattedMessage } from 'react-intl';

interface HeaderContentProps {
  title: string | undefined,
  iconLeft?: ReactNode,
  iconRight?: ReactNode,
  onClickLeft: () => void,
  onClickRight?: () => void,
  className?: string
}

const HeaderContent = (props: HeaderContentProps) => {
  const {title, iconLeft, iconRight, onClickLeft, onClickRight, className=''} = props;

  return (
    <div className={`flex items-center justify-between py-4 px-4 w-full ${className}`}>
        <div className="cursor-pointer" onClick={onClickLeft}>
            {(iconLeft && iconLeft)}
        </div>
        <FormattedMessage id={title}>
            {(message) => <span className="flex-1 text-center text-base text-white font-bold leading-5 tracking-wide">{message}</span>}
        </FormattedMessage>
        <div className="cursor-pointer" onClick={onClickRight}>
            {(iconRight && iconRight)}
        </div>
    </div>
  );
};

export default HeaderContent;