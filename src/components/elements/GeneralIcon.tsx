interface GeneralIconProps {
    IconElement: any,
    onClick?: () => void,
}

export const GeneralIcon = (props: GeneralIconProps) => {
    const { IconElement, onClick } = props;
    return (
        <div onClick={onClick}>
            <IconElement />
        </div>
    );
}