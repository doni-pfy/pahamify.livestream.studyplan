import React from 'react';  
import SyncLoaded from "react-spinners/SyncLoader";
import ReactPlayer from "react-player-pfy";
import * as utils from '../../utils/common';
import Slider, { createSliderWithTooltip } from 'rc-slider';
import 'rc-slider/assets/index.css';

const SliderWithTooltip = createSliderWithTooltip(Slider);

// import Iframe from 'react-iframe'

const ChevronDown = "https://d3ka6vo0n1gmoz.cloudfront.net/skillify/images/feature-courseDetail/icon_downArrow.svg";
const PlayButton = "https://d3ka6vo0n1gmoz.cloudfront.net/skillify/images/feature-courseDetail/icon_playButton.svg";
const PauseButton = "https://d3ka6vo0n1gmoz.cloudfront.net/skillify/images/feature-courseDetail/icon_pause1.svg";
const FullScreenButton = "https://d3ka6vo0n1gmoz.cloudfront.net/skillify/images/feature-courseDetail/icon_fullscreen.svg"
const SpeakerButton = "https://d3ka6vo0n1gmoz.cloudfront.net/skillify/images/feature-courseDetail/icon_speaker.svg";
const SpeedButton = "https://d3ka6vo0n1gmoz.cloudfront.net/skillify/images/feature-courseDetail/icon_speed.svg";

const zIndexes = {
    one: 5,
    two: 6,
    three: 7,
    four: 8,
    five: 9,
};

class VideoPlayer extends React.Component {
    constructor(props){
        super(props);
        this.state = {
            isHoverVideo: false,
            isVideoPlay: false,
            isFullScreen: false,
            isDestroy: this.props.isDestroy,
            isVideoError: this.props.isVideoError,
            volume: 0.8,
            internetSpeed: 100,
            playbackRate: '1x',
            sliderWidth: '30vw',
            sliderWidthFullScreen: '83vw',
            sliderMargin: '3vw',
            isVideoFinished: false,
            currentTime: 0,
            duration: 0,
        }
    }

    componentDidMount(){
        document.addEventListener("fullscreenchange", this.exitFullscreen.bind(this));
        window.addEventListener("resize", this.resize.bind(this));
        this.resize();
    }

    componentWillUnmount(){
        document.removeEventListener("fullscreenchange", this.exitFullscreen.bind(this));
        window.removeEventListener("resize", this.resize.bind(this));
    }

    resize() {
        if(window.innerWidth >= 1920){
            this.setState({
                sliderWidth: '56vw',
                sliderWidthFullScreen: '80vw',
                sliderMargin: '3vw',
            })
        } else if(window.innerWidth >= 1440){
            this.setState({
                sliderWidth: '73vw',
                sliderWidthFullScreen: '77vw',
                sliderMargin: '7vw'
            })
        } else {
            this.setState({
                sliderWidth: '52vw',
                sliderWidthFullScreen: '77vw',
                sliderMargin: '7vw'
            })
        }
	}

    exitFullscreen(event){
        if (!document.fullscreenElement) {
            // fullscreen is cancelled
            if(this.state.isFullScreen){
                this.onFullScreenPressed();
            }
        }
    }

    escFunction(event){
        if(event.keyCode === 27 && this.state.isFullScreen){
            this.onFullScreenPressed();
        }
    }

    onFullScreenPressed(){
        var x = document.getElementsByTagName("nav");
        if(this.state.isFullScreen){
            if (document.fullscreenElement) document.exitFullscreen();
            x[0].style.zIndex = zIndexes.four;
            this.setState({
                isFullScreen: false,
            })
        } else {
            document.documentElement.requestFullscreen();
            x[0].style.zIndex = 1;
            this.setState({
                isFullScreen: true,
            })
        }
    }

    onErrorVideo(err) {
		// this.props.onErrorVideo(err);
		if(err){
			console.log("errorPlayingVideo", err);
		}
    }

    onRefreshVideo(){
		this.setState({
			isVideoError: false
		})
        // this.onPressVid(this.state.videoSelectedIndex);
        this.props.onRefreshVideo();
	}
    
    onVideoReady() {
        if (global.times > 0){
            this.videRef.seekTo(parseInt(global.times));
            global.times = 0;
        }
        this.setState({
            duration: Math.round(this.videRef.getDuration())
        });
    }

	onload(startTime){
		var downloadSize = 4995374;
		var endTime = (new Date()).getTime();
		var duration = (endTime - startTime) / 1000;
		var bitsLoaded = downloadSize * 8;
		var speedBps = Math.round(bitsLoaded / duration);
		var speedKbps = (speedBps / 1024).toFixed(2);
        var speedMbps = (speedKbps / 1024).toFixed(2);
        
		this.setState({
			internetSpeed: speedMbps
        })
        // this.bitrateHandling(speedMbps);
		console.log('speed', speedMbps + " Mbps");
    }
    
    bitrateHandling(speedMbps){
        if(speedMbps >= 10) {
            console.log('720p');
            if(this.props.selectedResolution != '720p'){
                this.onResolutionSelected('720p');
            }
        } else if(speedMbps >= 5) {
            console.log('480p')
            if(this.props.selectedResolution != '480p'){
                this.onResolutionSelected('480p');
            }
        } else if (speedMbps < 5) {
            console.log('360p')
            if(this.props.selectedResolution != '360p'){
                this.onResolutionSelected('360p');
            }
        }
    }
    
    onVideoPaused(){
		this.setState({
			isVideoPlay: false
		})
	}

	onVideoPlayed(){
		this.setState({
			isVideoPlay: true
		})
    }

    onPlayVideo(){
        this.setState({
			isVideoPlay: true
		})
    }
    
    onPauseVideo(){
        this.setState({
            isVideoPlay: false
        })
    }

    onVideoProgress(data){
        this.setState({
            currentTime: data.playedSeconds
        })
    }

    onChange(data){
        if(data !== null || data !== undefined){
            if(this.videRef){
                this.videRef.seekTo(parseInt(data));
            }
            this.setState({
                currentTime: data
            })
        }
    }

    onChangeVolume(data){
        this.setState({
            volume: parseFloat(data/100.0)
        })
    }

    updateState(){
        this.setState({
            isDestroy: this.props.isDestroy,
            isVideoError: this.props.isVideoError,
            videoLink: this.props.videoLink,
        })
    }

    onResolutionSelected(data){
        global.times = this.videRef.getCurrentTime();
        this.props.onResolutionSelected(data);
    }

    onSpeedPressed() {
        this.setState({
            isSpeed: true,
        }, () => {
            this.onPauseVideo()
        })
    }

    onSpeedSelected(speed) {
        this.setState({
            playbackRate: speed,
            isSpeed: false
        }, ()=> {
            this.onPlayVideo();
        })
    }

    percentFormatter(v) {
        return `${utils.secToMinutes(v)}`;
    }

    percentFormatterVolume(v) {
        return `${v} %`;
    }

    renderController(){
        const rawData = ['480p', '720p'];
        let categoriesTiles = [];
        for(let i=0; i<rawData.length; i++){
            categoriesTiles.push(
                <div
                    key={Math.random()}
                    className="touchable"
                    onClick={this.onResolutionSelected.bind(this, rawData[i])}
                    style={{ marginBottom: (i === rawData.length - 1) ? "8px" : "0px", color: '#000', fontSize: 8, zIndex: zIndexes.one}}
                    >
                    {rawData[i]}
                    {rawData.length-1 != i && <hr className='underline' style={{border: '0.5px solid var(--navbar-greylight)', width: 'auto', marginBottom: '4px', marginTop: '4px'}} />}

                </div>
            );
        }

        let playButton;
        if(this.state.isVideoPlay){
            playButton = (
                <img
                    onClick={this.onPauseVideo.bind(this)}
                    src={PauseButton}
                    className="touchable"
                    style={{ height: this.props.platform === 'mobile' ? 60 : "100px", width: this.props.platform === 'mobile' ? 60 : "100px"}}
                />
            )
        } else {
            playButton =(
                <img
                    onClick={this.onPlayVideo.bind(this)}
                    src={PlayButton}
                    className="touchable"
                    style={{ height: this.props.platform === 'mobile' ? 60 : "100px", width: this.props.platform === 'mobile' ? 60 : "100px"}}
                />
            )
        }

        const speedRawData = ['0.5x', '1x', '1.5x', '2x'];
        let speedTiles = [];
        for(let i=0; i<speedRawData.length; i++){
            speedTiles.push(
                <div
                    key={Math.random()}
                    className="touchable"
                    onClick={this.onSpeedSelected.bind(this, speedRawData[i])}
                    style={{ marginBottom: (i === rawData.length - 1) ? "8px" : "0px", color: '#000', fontSize: 14, backgroundColor: this.state.playbackRate == speedRawData[i] && '#babec4', borderRadius: 8,  }}
                    >
                    {speedRawData[i]}
                </div>
            );
        }

        if(this.state.isHoverVideo || !this.state.isVideoPlay) {
            return(
                <div>
                    <div style={{position: 'absolute', justifyContent: 'center', alignItems: 'center', display: 'flex', top: 0, bottom: 0, right: 0, left: 0, zIndex: zIndexes.two}}>
                        {playButton}
                        <div style={{ width: this.props.platform === 'mobile' ? '50%' : '30%', height: 'auto', backgroundColor: 'rgba(255, 255, 255, 0.9)', position: 'absolute', borderRadius: 8, display: this.state.isSpeed ? 'flex' : 'none', flexDirection: 'column', padding: 8 }}>
                            <a
                                style={{ alignSelf: "center", justifySelf: 'center', fontSize: 14, color: '#000', marginBottom: 8, }}
                            >
                                Playback Rate
                            </a>
                            {speedTiles}
                        </div>
                        <div style={{ display: 'flex', flex: 1, flexDirection: 'row', alignItems: 'flex-end', position: 'absolute', bottom: 10, left: 15, width: '95%'}}>
                            <div style={{display: 'flex', flexDirection: 'row', width: "100%"}}>
                                <div style={{ position: 'relative', marginRight: this.props.platform === 'mobile' ? 4  : 10, maxWidth: 50, minWidth: this.props.platform === 'mobile' ? 37 : 50 }}>
                                    <a
                                        style={{ color: '#fff', position: 'absolute', left: 0, fontSize: this.props.platform === 'mobile' && 12 }}
                                    >
                                        {this.state.currentTime ? utils.secToMinutes(parseInt(this.state.currentTime)) : '00:00'}
                                    </a>
                                </div>
                                <SliderWithTooltip
                                    min={0}
                                    max={this.state.duration}
                                    value={this.state.currentTime}
                                    tipFormatter={this.percentFormatter}
                                    tipProps={{ overlayClassName: 'foo' }}
                                    onChange={this.onChange.bind(this)}
                                    disabled={false}
                                    style={{
                                        display: 'flex',
                                        flex: 1,
                                        marginRight: '10px',
                                        alignSelf: 'center',
                                        justifySelf: 'center',
                                    }}
                                />
                                <a
                                    style={{ alignSelf: "center", justifySelf: 'center', marginRight: '10px', color: '#fff', fontSize: this.props.platform === 'mobile' && 12 }}
                                >
                                    {this.state.duration ? utils.secToMinutes(this.state.duration) : '00:00'}
                                </a>
                            </div>


                            <div onClick={this.onSpeedPressed.bind(this)} className="touchable" style={{zIndex: zIndexes.three, marginRight: 8}} >
                                <img
                                    src={SpeedButton}
                                    className="touchable"
                                    style={{ height: this.props.platform === 'mobile' ? 25 : 33, width: this.props.platform === 'mobile' ? 25 : 35}}
                                />
                            </div>

                            <div className="dropdown" style={{zIndex: zIndexes.three}} 
                                onMouseEnter={() => {
                                    this.setState({
                                        isVolume: true
                                    });
                                }}
                                onMouseLeave={() => {
                                    this.setState({
                                        isVolume: false
                                    });
                                }} >
                                {this.state.isVolume && (
                                    <div style={{ backgroundColor: 'white', padding: 8, maxWidth: '30px', borderTopLeftRadius: '8px', borderTopRightRadius: '8px', borderBottomLeftRadius:0, borderBottomRightRadius: 0}}>
                                        <SliderWithTooltip
                                            min={0}
                                            max={100}
                                            value={this.state.volume * 100}
                                            vertical
                                            tipFormatter={this.percentFormatterVolume}
                                            tipProps={{ overlayClassName: 'foo' }}
                                            onChange={this.onChangeVolume.bind(this)}
                                            disabled={false}
                                            style={{
                                                height: '100px',
                                            }}
                                        />
                                    </div>
                                )}
                                <img
                                    src={SpeakerButton}
                                    className="touchable"
                                    style={{ height: this.props.platform === 'mobile' ? 20 : "30px", width: this.props.platform === 'mobile' ? 20 : "30px"}}
                                />
                            </div>

                            <div className="dropdown" style={{marginLeft: '10px', maxWidth: '10%', zIndex: zIndexes.three,}} 
                                onMouseEnter={() => {
                                    this.setState({
                                        isResolution: true
                                    });
                                }}
                                onMouseLeave={() => {
                                    this.setState({
                                        isResolution: false
                                    });
                                }} >
                                {this.state.isResolution && (
                                    <div style={{ display: 'block', backgroundColor: 'white', padding: 8, width: this.props.platform === 'mobile' ? 40 : 'inherit', borderTopLeftRadius: '8px', borderTopRightRadius: '8px', borderBottomLeftRadius:0, borderBottomRightRadius: 0}}>{categoriesTiles}</div>
                                )}
                                <div
                                    style={{
                                        position: 'relative',
                                        border: "2px solid white",
                                        display: "flex",
                                        justifyContent: "center",
                                        alignItems: "center",
                                        maxWidth: !this.props.platform === 'mobile' && "100%",
                                        height: '100%',
                                        color: 'white',
                                        backgroundColor: 'transparent',
                                        fontSize: '17px',
                                        paddingLeft: '5px',
                                        paddingRight: '5px',
                                        width: this.props.platform === 'mobile'&& 40
                                    }}
                                >
                                    <span style={{ flex: 1, marginRight: "6px",fontSize: this.props.platform === 'mobile' && 12 }}>{this.props.selectedResolution}</span>
                                    {!this.props.platform === 'mobile' && (
                                        <img className="chevron-dropdown" src={ChevronDown}/>
                                    )}
                                </div>
                            </div>
                            <img
                                onClick={this.onFullScreenPressed.bind(this)}
                                src={FullScreenButton}
                                className="touchable"
                                style={{ height: this.props.platform === 'mobile' ? 20 : "30px", width: this.props.platform === 'mobile' ? 25 : "30px", marginLeft: '10px'}}
                            />
                        </div>
                    </div>
                </div>
            )
        }

        return <div />
    }

    renderFullScreen() {
        if(this.props.isDestroy){
            return (
                <div
                        style={{
                            width: "100vw",
                            height: "100vh",
                            zIndex: zIndexes.five,
                            backgroundColor: "#000",
                            position: "fixed",
                            top: 0,
                            bottom: 0,
                            left: 0,
                            display: 'flex',
                            alignItems: 'center',
                            justifyContent: 'center',
                        }}
                    >
                        <SyncLoaded 
                            size={25}
                            color={"#123abc"}
                            loading={true}
                        />
                    </div>
            )
        } else if (this.props.isVideoError || !this.props.videoUrl){
            return (
                <div
                        style={{
                            width: "100vw",
                            height: "100vh",
                            zIndex: zIndexes.five,
                            position: "fixed",
                            top: 0,
                            bottom: 0,
                            left: 0,
                            backgroundColor: "#000",
                            display: 'flex',
                            alignItems: 'center',
                            justifyContent: 'center',
                            flexDirection: 'column'
                        }}
                    >
                        <span style={{ color: '#fff' }}  className="description-title passthrough">
                            Mohon tunggu sebentar … 
                        </span>
                        <div
                            onClick={this.onRefreshVideo.bind(this)}
                            className="btn-course-category-inactive touchable passthrough"
                            style={{
                                display: "inline-block",
                                justifyContent: "center",
                                alignItems: "center",
                                marginTop: '10px',
                                color: 'white',
                                border: '1px solid #fff',
                                textAlign: 'center'  
                            }}
                        >
                            muat ulang
                        </div>
                    </div>
            )
        } else {
            return (
                <div 
                        style={{ 
                            width: '100vw', 
                            height:"100vh", 
                            backgroundColor: '#000',
                            position: 'fixed',
                            zIndex: zIndexes.five,
                            top: 0,
                            bottom: 0,
                            left: 0,
                        }} 
                        onMouseEnter={() => {
                            this.setState({
                                isHoverVideo: true
                            });
                        }}
                        onMouseLeave={() => {
                            this.setState({
                                isHoverVideo: false
                            });
                        }}>
                        {this.renderController()}
                        <ReactPlayer
                            height="100%"
                            width="100vw"
                            style={{
                                backgroundColor: "#000",
                                zIndex: zIndexes.one,
                                position: "fixed",
                                top: 10,
                                bottom: 0,
                                left: 0,
                            }}
                            // controls
                            url={this.props.videoUrl}
                            playing={this.state.isVideoPlay}
                            onError={this.onErrorVideo.bind(this)}
                            onReady={this.onVideoReady.bind(this)}
                            onPause={this.onVideoPaused.bind(this)}
                            onPlay={this.onVideoPlayed.bind(this)}
                            onProgress={this.onVideoProgress.bind(this)}
                            volume={this.state.volume}
                            file={{forceDASH: true}}
                            playbackRate={parseFloat(this.state.playbackRate.substring(0, this.state.playbackRate.length-1))}
                            ref={(ref)=> {this.videRef = ref}}
                        />
                    </div>
            )
        }
    }

    render() {
        const rawData = ['480p', '720p'];
        let categoriesTiles = [];
        for(let i=0; i<rawData.length; i++){
            categoriesTiles.push(
                <div
                    key={Math.random()}
                    className="touchable"
                    onClick={this.onResolutionSelected.bind(this, rawData[i])}
                    style={{ marginBottom: (i === rawData.length - 1) ? "8px" : "0px", color: '#000', fontSize: 12}}
                    >
                    {rawData[i]}
                    {rawData.length-1 != i && <hr className='underline' style={{border: '0.5px solid var(--navbar-greylight)', width: 'auto', marginBottom: '4px', marginTop: '4px'}} />}

                </div>
            );
        }

        if(this.state.isFullScreen){
            return this.renderFullScreen();
        }

        if(this.props.isDestroy){
            return (
                <div
                        style={{
                            width: this.props.platform === 'mobile' ? '90vw' : '100%', 
                            height: this.props.platform === 'web' ? '80vh' : '35vh',
                            zIndex: zIndexes.one,
                            backgroundColor: "#000",
                            display: 'flex',
                            alignItems: 'center',
                            justifyContent: 'center',
                            borderRadius: 8
                        }}
                    >
                        <SyncLoaded 
                            size={ this.props.platform === 'mobile' ? 10 : 25}
                            color={"#123abc"}
                            loading={true}
                        />
                    </div>
            )
        } else if (this.props.isVideoError || !this.props.videoUrl){
            return (
                <div
                        style={{
                            width: '100%', 
                            height: this.props.platform === 'web' ? '80vh' : '35vh',
                            zIndex: zIndexes.one,
                            backgroundColor: "#000",
                            display: 'flex',
                            alignItems: 'center',
                            justifyContent: 'center',
                            flexDirection: 'column',
                            borderRadius: 8
                        }}
                    >
                        <span className="description-title passthrough" style={{ color: '#fff' }}>
                            Mohon tunggu sebentar …
                        </span>
                        <div
                            onClick={this.onRefreshVideo.bind(this)}
                            className="btn-course-category-inactive touchable passthrough"
                            style={{
                                display: "inline-block",
                                justifyContent: "center",
                                alignItems: "center",
                                marginTop: '10px',
                                color: 'white',
                                border: '1px solid #fff',
                                textAlign: 'center'  
                            }}
                        >
                            muat ulang
                        </div>
                    </div>
            )
        } else {
            return (
                <div 
                        style={{ 
                            width: '100%', 
                            height:'80%', 
                            backgroundColor: '#000',
                            position: 'relative',
                        }} 
                        onMouseEnter={() => {
                            this.setState({
                                isHoverVideo: true
                            });
                        }}
                        onMouseLeave={() => {
                            this.setState({
                                isHoverVideo: false
                            });
                        }}>
                        {this.renderController()}
                        <ReactPlayer
                            height="100%"
                            width="100%"
                            style={{
                                backgroundColor: "#000",
                                zIndex: zIndexes.one,
                                position: "relative",
                                borderRadius: 8
                            }}
                            // controls
                            url={this.props.videoUrl}
                            playing={this.state.isVideoPlay}
                            onError={this.onErrorVideo.bind(this)}
                            onReady={this.onVideoReady.bind(this)}
                            onPause={this.onVideoPaused.bind(this)}
                            // onPlay={this.onVideoPlayed.bind(this)}
                            onProgress={this.onVideoProgress.bind(this)}
                            volume={this.state.volume}
                            file={{forceDASH: true}}
                            playbackRate={parseFloat(this.state.playbackRate.substring(0, this.state.playbackRate.length-1))}
                            ref={(ref)=> {this.videRef = ref}}
                        />
                    </div>
            )
        }
    }
}

export default VideoPlayer;