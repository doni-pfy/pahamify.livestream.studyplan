import React, { ReactNode } from "react";
import LoginInfoContainer from "./LoginInfoContainer";
import { GeneralButton } from "src/components/elements/GeneralButton";
import { useRouter } from "next/router";

interface HeaderMainProps {
  logo: ReactNode;
  isLogin: boolean
}

const HeaderMain = (props: HeaderMainProps) => {
  const { logo, isLogin } = props;
  const router = useRouter()

  return (
    <div className={`flex items-center justify-between py-2 px-4 w-full`}>
      <div>{logo}</div>
      <GeneralButton size="sm" textId="button.text.studyplan.header" className={`mt-2 bg-white rounded-full base-primary mr-2`} textColor="text-base-primary"
        onClick={() => {
          router.push('studyplan')
        }}
      />
    </div>
  );
};

export default HeaderMain;
