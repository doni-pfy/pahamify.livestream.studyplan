import CaretDown from 'public/caret-down.svg';
import PhysicsIcon from 'public/icon-course/fisika.svg';
import ChemistryIcon from 'public/icon-course/kimia.svg';
import MathIcon from 'public/icon-course/mtk.svg';
import React, { useState } from 'react';
import { FormattedMessage } from 'react-intl';
import { Major } from 'src/constants/subject';
import styles from 'src/styles/SubjectDropdown.module.scss';
import { Text } from './Text';
import { TextWithImageIcon } from './TextWithImageIcon'
import { StudyPlanDetailsAccordionSubtitle } from './StudyPlanDetailsAccordionSubtitle'
import { StudyPlanAccordionHeaderTitle } from './StudyPlanAccordionHeaderTitle'
import { StudyPlanDetailsAccordionListItem } from './StudyPlanDetailsAccordionListItem'

import { useRouter } from "next/router";

interface StudyPlanDetailsAccordionProps {
    onSelected?: (subjectId: number) => void,
    open?: boolean,
    className?: String
}

export const StudyPlanDetailsAccordion = (props: StudyPlanDetailsAccordionProps) => {
    const [isShowDropdown, setShowDropdown] = useState<boolean>(false);
    const [selectedItem, setSelectedItem] = useState<number>(0);
    const {onSelected, open} = props;

    function onDropdownClicked(textId: number) {
        setSelectedItem(textId);
        onSelected?.(textId);
        setShowDropdown(!isShowDropdown);
    }

    const router = useRouter()

    const chapter = router.query.chapter

    if(!chapter) { return (
      <div></div>
    )}

    const chapterData = JSON.parse(chapter as string)

    return (
        <>
        {
            !open ? (
            <div className="mx-4">
                {
                    chapterData.chapterList.map((data: any) => (
                        <div key={data.courseId}>
                            <div className="mb-8">
                                <StudyPlanDetailsAccordionSubtitle courseTitle={data.chapterTitle} difficulty={data.kesulitan ? Math.round(data.kesulitan * 10) / 10 : 0}/>
                            </div>
                            {
                                data.actionList.map((action: any) => (
                                <StudyPlanDetailsAccordionListItem chapterData={chapter} chapterTitle={action.title} frequency={0} progress={0} />
                                ))
                            }
                        </div>
                    ))
                }
            </div>
            ) : (
                <div></div>
            )
        }
        
        </>
    );
}
