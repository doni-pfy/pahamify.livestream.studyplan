import XButton from 'public/x-button.svg';
import React, { ReactNode, useEffect, useRef } from "react";
import { IconButton } from "src/components/elements/IconButton";

type AnyEvent = MouseEvent | TouchEvent;

interface GeneralModalProps {
    onClose: () => void,
    children: ReactNode,
    className?: string,
    withoutCloseButton?: boolean,
    whiteBG?: boolean,
}

export const GeneralModal = (props: GeneralModalProps) => {
    const { withoutCloseButton, whiteBG, className, children, onClose } = props;
    const node = useRef<HTMLDivElement>(null);

    useEffect(() => {
        document.addEventListener("mousedown", handleClick);
        return () => {
            document.removeEventListener("mousedown", handleClick);
        }
    }, []);

    const handleClick = (e: AnyEvent) => {
        const el = node.current;
        if (!el || el.contains(e.target as Node)) return;
        else onClose();
    }

    return (
        <React.Fragment>
            {/* Float card */}
            <div className="container mx-auto justify-center items-center flex overflow-x-hidden overflow-y-auto fixed inset-0 outline-none focus:outline-none" style={{zIndex: 61}}>
                {/* Content */}
                <div ref={node} className={`animate-fade-in-up m-4 p-6 border-0 rounded-xl shadow-sm relative flex flex-col items-center w-full bg-white outline-none focus:outline-none ${className}`}>
                    {withoutCloseButton ? null : <IconButton onClick={() => onClose()} IconElement={XButton} className="absolute -top-2 -right-2" /> }
                    {children}
                </div>
            </div>
            {/* Darken background */}
            <div className={`opacity-10 fixed inset-0 ${whiteBG ? "bg-white opacity-40" : "bg-black opacity-25"}`} style={{zIndex: 60}}></div>
        </React.Fragment>
    );
}