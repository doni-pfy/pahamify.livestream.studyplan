import { FormattedMessage } from 'react-intl';
import Link from "next/link";

interface TextWithIconText {
    IconElement: any,
    textId: string,
    className?: String,
    onClick?: () => void,
    routerPath?: string
}

export const TextWithIcon = (props: TextWithIconText) => {
    const { IconElement, textId, className, onClick, routerPath } = props;
    return (
        <FormattedMessage id={textId}>
            {(message) => (
                <div onClick={onClick} className={`${className} flex items-center`}>
                    {routerPath? (
                        <Link href={routerPath}>
                            <a className="contents">
                                <IconElement  className="z-1"/>
                                <span className="text-sm ml-4 font-bold leading-4 tracking-wide">
                                    {message}
                                </span>
                            </a>
                        </Link>
                    ): (
                        <div className="contents">
                            <IconElement className="z-1"/>
                            <span className="text-sm ml-4 font-bold leading-4 tracking-wide">
                                {message}
                            </span>
                        </div>
                    )}
                </div>)}
        </FormattedMessage>
    );
}