import { FormattedMessage } from 'react-intl';
import MathIcon from 'public/icon-course/mtk.svg';

import { Text } from './Text';

interface SectionHeaderProps {
    className?: string,
    title: string,
    descriptionId: string,
    solidDesc?: boolean,
}

export const StudyPlanDetailsHeaderTitle = (props: SectionHeaderProps) => {
    const { className, title, descriptionId, solidDesc } = props;
    
    return (
        <div className={`flex flex-col bg-gray-200 items-center rounded-lg ${className}`}>
            <div className="p-4 pb-2 px-5 flex flex-col justify-items-center justify-center items-center">
               <MathIcon/>
            </div>
        </div>
    );
}