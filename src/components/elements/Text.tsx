import { FormattedMessage } from 'react-intl';
import Link from "next/link";

interface TextWithIconText {
    textId?: string,
    text?: string,
    className?: String,
    textClassName?: String,
    onClick?: () => void,
    routerPath?: string
}

export const Text = (props: TextWithIconText) => {
    const { text, textId, textClassName, className, onClick, routerPath } = props;
    return !textId ? (
                <div onClick={onClick} className={`${className} flex items-center`}>
                    {routerPath? (
                        <Link href={routerPath}>
                            <a className="contents">
                                <span className={`${textClassName} text-sm ml-2 leading-4 tracking-wide`}>
                                    {text}
                                </span>
                            </a>
                        </Link>
                    ): (
                        <div className="contents">
                            <span className={`${textClassName} text-sm ml-2 leading-4 tracking-wide`}>   
                                {text}
                            </span>
                        </div>
                    )}
                </div>
    ):  (
        <FormattedMessage id={textId}>
            {(message) => (
                <div onClick={onClick} className={`${className} flex items-center`}>
                    {routerPath? (
                        <Link href={routerPath}>
                            <a className="contents">
                                <span className="text-sm ml-2 font-bold leading-4 tracking-wide">
                                    {message}
                                </span>
                            </a>
                        </Link>
                    ): (
                        <div className="contents">
                            <span className="text-sm ml-2 font-bold leading-4 tracking-wide">   
                                {message}
                            </span>
                        </div>
                    )}
                </div>)}
        </FormattedMessage>
    )
}