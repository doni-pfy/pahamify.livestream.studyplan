import CaretDown from 'public/caret-down.svg';
import PhysicsIcon from 'public/icon-course/fisika.svg';
import ChemistryIcon from 'public/icon-course/kimia.svg';
import MathIcon from 'public/icon-course/mtk.svg';
import React, { useState } from 'react';
import { FormattedMessage } from 'react-intl';
import { Major } from 'src/constants/subject';
import styles from 'src/styles/SubjectDropdown.module.scss';
import { Text } from './Text';
import { TextWithImageIcon } from './TextWithImageIcon'

interface StudyPlanAccordionSubtitleProps {
    onSelected?: (subjectId: number) => void,
    className?: String
}

export const StudyPlanAccordionHeaderTitle = (props: StudyPlanAccordionSubtitleProps) => {
    const [isShowDropdown, setShowDropdown] = useState<boolean>(false);
    const [selectedItem, setSelectedItem] = useState<number>(0);
    const {onSelected} = props;

    function onDropdownClicked(textId: number) {
        setSelectedItem(textId);
        onSelected?.(textId);
        setShowDropdown(!isShowDropdown);
    }

    return (
        <div className="my-4 ml-6 pb-2 relative flex flex-row" style={{borderBottom: "1px solid rgba(225,232,242,0.9)"}}>
            <Text className="w-1/2" textClassName="text-xs opacity-70" onClick={() => {}} text="15 Bab Besar" />
            <Text className="w-1/3" textClassName="text-xs opacity-70" onClick={() => {}} text="Frekuensi" />
            <Text className="w-1/3" textClassName="text-xs opacity-70" onClick={() => {}} text="Tingkat Kesulitan" />
        </div>
    );
}
