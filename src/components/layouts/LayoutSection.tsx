import React, { ReactNode } from "react";
import { FormattedMessage } from "react-intl";

interface LayoutSectionProps {
  children: ReactNode;
  title: string;
  description?: string;
  solidDesc?: boolean;
}

const LayoutSection = (props: LayoutSectionProps) => {
  const { children, title, description, solidDesc } = props;
  const descStyle = solidDesc? `` : `text-opacity-70`;

  return (
    <div className="mb-12">
      <div className="mb-6">
        <FormattedMessage id={title}>
          {(message) => <h2 className="text-text-primary text-center px-4 mb-1 text-lg font-bold ls-0.025">{message}</h2>}
        </FormattedMessage>
        {
          description ? (
            <FormattedMessage id={description}>
              {(message) => <p className={`text-text-primary text-center px-4 text-sm ls-0.025 ${solidDesc}`}>{message}</p>}
            </FormattedMessage>
          ):null
        }
      </div>

      {children}
    </div>
  );
};

export default LayoutSection;
