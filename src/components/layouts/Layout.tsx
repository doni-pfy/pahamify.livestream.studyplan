import React, { ReactNode, useEffect, useState } from "react";
import { useRouter } from "next/router";

import Header from "../modules/Header";
import Footer from "../elements/Footer";
import BottomNav from "../modules/BottomNav";
import { Path } from "src/constants/path";
import { TransparentHeader } from "../modules/TransparentHeader";
import { useAppState } from "src/context/appstate.context";
import { AppStateActionType } from "src/model/appstate";
import LoginModal from "../modules/LoginModal";

interface LayoutProps {
  children: ReactNode;
}

const Layout = ({ children }: LayoutProps) => {
  const router = useRouter();
  const { state: { appState }, dispatch: dispatchAppState } = useAppState();
  const [hasFooter, sethasFooter] = useState<Boolean>(true);
  const [hasBottomNav, setHasBottomNav] = useState<Boolean>(true);
  const [hasHeader, setHasHeader] = useState<Boolean>(true);

  useEffect(() => {
    setHasHeader(!Path.TransparentHeaderPath.includes(router.pathname))
    sethasFooter(!Path.ExcludeFooterPath.includes(router.pathname))
    setHasBottomNav(!Path.ExcludeBottomNavPath.includes(router.pathname))
  }, [router.pathname]);

  return (
    <div className="container mx-auto min-h-screen flex flex-col bg-background">
      {hasHeader ? <Header /> : <TransparentHeader />}
      {children}
      {hasFooter && <Footer />}
      {hasBottomNav && <BottomNav />}
      { appState.isModalLoginOpen &&
        <LoginModal
          title="card.title.login"
          desc="card.text.login"
          separatorText="separator.login"
          onClose={() => dispatchAppState({
            type: AppStateActionType.SetModalLogin,
            payload: { ...appState, isModalLoginOpen: false }
          })}
        />
      }
    </div>
  );
};

export default Layout;
