module.exports = {
  purge: ['./pages/**/*.{js,ts,jsx,tsx}', './src/components/**/*.{js,ts,jsx,tsx}'],
  darkMode: false, // or 'media' or 'class'
  theme: {
    extend: {
      colors: {
        'base-primary': ({ opacityVariable, opacityValue }) => {
          if (opacityValue !== undefined) {
            return `rgba(var(--color-base), ${opacityValue})`
          }
          if (opacityVariable !== undefined) {
            return `rgba(var(--color-base), var(${opacityVariable}, 1))`
          }
          return `rgb(var(--color-base))`
        },
        'base-secondary': ({ opacityVariable, opacityValue }) => {
          if (opacityValue !== undefined) {
            return `rgba(var(--color-base-secondary), ${opacityValue})`
          }
          if (opacityVariable !== undefined) {
            return `rgba(var(--color-base-secondary), var(${opacityVariable}, 1))`
          }
          return `rgb(var(--color-base-secondary))`
        },
        'base-tertiary': ({ opacityVariable, opacityValue }) => {
          if (opacityValue !== undefined) {
            return `rgba(var(--color-base-tertiary), ${opacityValue})`
          }
          if (opacityVariable !== undefined) {
            return `rgba(var(--color-base-tertiary), var(${opacityVariable}, 1))`
          }
          return `rgb(var(--color-base-tertiary))`
        },
        'text-primary': ({ opacityVariable, opacityValue }) => {
          if (opacityValue !== undefined) {
            return `rgba(var(--color-text), ${opacityValue})`
          }
          if (opacityVariable !== undefined) {
            return `rgba(var(--color-text), var(${opacityVariable}, 1))`
          }
          return `rgb(var(--color-text))`
        },
        'card-primary': '#E0E8F3',
        'card-secondary': '#EAEFF7',
        'background': '#F4F7FA',
        'green': '#2ECF2F',
        'green-200': '#D0E731',
        'red': '#E54C6E',
        'orange': '#FF6724',
        'orange-200': '#ED8D56',
        'yellow': '#FFCB64',
      },
      keyframes: {
        'fade-in-up': {
          'from': {
            opacity: '0',
            transform: 'translateY(10px)',
          },
          'to': {
            opacity: '1',
            transform: 'translateY(0)',
          }
        },
        'fade-in': {
          'from': {
            opacity: '0',
          },
          'to': {
            opacity: '1',
          }
        }
      },
      animation: {
        'fade-in-up': 'fade-in-up 0.3s ease-out',
        'fade-in': 'fade-in 0.3s ease'
      }
    },
  },
  variants: {
    extend: {
      backgroundColor: ['checked'],
      borderColor: ['checked'],
    },
  },
  corePlugins: {
    container: false
  },
  plugins: [
    function ({ addComponents }) {
      addComponents({
        '.container': {
          width: '100%',
          // marginLeft: 'auto',
          // marginRight: 'auto',
          // paddingLeft: '2rem',
          // paddingRight: '2rem',
          '@screen sm': {
            maxWidth: '640px',
          },
          '@screen md': {
            maxWidth: '720px',
          },
          '@screen lg': {
            maxWidth: '720px',
          },
          '@screen xl': {
            maxWidth: '720px',
          },
        }
      })
    }
  ],
}
