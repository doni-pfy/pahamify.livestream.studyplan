FROM node:14.16.1-alpine
ARG ENV
ENV NODE_ENV=production
ENV ENV=${ENV}
RUN apk add --no-cache bash
RUN apk add --update curl
RUN apk add dos2unix --update-cache --repository http://dl-3.alpinelinux.org/alpine/edge/community/ --allow-untrusted
WORKDIR /usr/src/app
COPY package*.json ./
RUN npm install --only=dev
RUN npm install
COPY . .
EXPOSE 3000
RUN dos2unix run.sh && apk del dos2unix
RUN npm run build
CMD /bin/bash ./run.sh	
