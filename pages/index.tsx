import dayjs, { Dayjs } from 'dayjs';
import InfoIcon from 'public/info.old.svg';
import React, { useEffect, useState } from "react";
import "slick-carousel/slick/slick-theme.css";
import "slick-carousel/slick/slick.css";
import Footer from 'src/components/elements/Footer';
import { TextWithIcon } from "src/components/elements/TextWithIcon";
import PageHeading from "src/components/modules/PageHeading";
import { Testimoni } from "src/constants/testimoni";
import { useUser } from "src/context/user.context";


import styles from "src/styles/Index.module.scss";


const Home = () => {
  const [testimoni, setTestimoni] = useState(Testimoni.Content);
  const { state: { user } } = useUser();
  const [jurusan, setJurusan] = useState('SAINTEK')
  const [conceptLearningTime, setConceptLearningTime] = useState(10);
  const [rangkumanLearningTime, setRangkumanLearningTime] = useState(10);
  const [jumlahSoal, setJumlahSoal] = useState(15);
  const [persoalLearningTime, setPersoalLearningTime] = useState(10);
  const [keyakinanValue, setKeyakinan] = useState(100);

  const [dayToUtbk, setDayToUtbk] = useState(0);
  const [hourPerDay, setHourPerDay] = useState(0);

  const totalSubBabUmum = 138;
  const totalSubBabSaintek = 162;
  const totalSubBabSoshum = 180;

  const utbkDate = dayjs("04-11-2022");

  function calculateLearningDebt() {
    if (dayToUtbk !== 0) {
      const jurusanSubab = jurusan === 'SAINTEK' ? totalSubBabSaintek : totalSubBabSoshum;
      const totalSubBab = totalSubBabUmum + jurusanSubab;

      const lamaBelajar = conceptLearningTime + rangkumanLearningTime + (jumlahSoal * persoalLearningTime);

      const totalMenitBelajar = totalSubBab * lamaBelajar;
      const totalJam = (totalMenitBelajar / 60) / (keyakinanValue / 100);
      setHourPerDay(totalJam / dayToUtbk);
    }
  }

  useEffect(() => {
    calculateLearningDebt();
  }, [jurusan, conceptLearningTime, rangkumanLearningTime, jumlahSoal, persoalLearningTime, dayToUtbk, keyakinanValue])

  function changeJurusan(indxJurusan: number) {
    if (indxJurusan === 0) { setJurusan('SAINTEK') }
    else if (indxJurusan === 1) { setJurusan('SOSHUM') }
  }
  const matpelDataUmum = [
    { subject: "Penalaran Umum", bab: 14, subBab: 42 },
    { subject: "Pengetahuan dan Pemahaman Umum", bab: 12, subBab: 36 },
    { subject: "Pemahaman Bacaan dan Menulis", bab: 7, subBab: 21 },
    { subject: "Kemampuan Kuantitatif", bab: 13, subBab: 39 },
  ]

  const matpelDataSaintek = [
    { subject: "Matematika", bab: 20, subBab: 60 },
    { subject: "Fisika", bab: 9, subBab: 27 },
    { subject: "Kimia", bab: 14, subBab: 42 },
    { subject: "Biologi", bab: 11, subBab: 33 },
  ]

  const matpelDataSoshum = [
    { subject: "Geografi", bab: 14, subBab: 54 },
    { subject: "Sejarah", bab: 12, subBab: 45 },
    { subject: "Sosiologi", bab: 7, subBab: 48 },
    { subject: "Ekonomi", bab: 13, subBab: 33 },
  ]

  return (
    <main className="w-full flex flex-col">
      <PageHeading onSelectJurusan={changeJurusan} className="pt-16" title="home.page.heading.title" description="home.page.heading.description" />
      <div className="mt-min-12 px-4 pt-3 pb-0 bg-background rounded-lg rounded-b-none">
        <div className="text-lg mt-4 flex text-base-primary text-opacity-70 justify-between">
          <h3 className="flex-1">Mata Pelajaran</h3>
          <h3 className="flex-1 text-center">Bab Besar</h3>
          <h3 className="flex-1 text-center">Sub Bab</h3>
        </div>
        {
          matpelDataUmum.map((item, idx) => {
            return (
              <div key={idx} className="flex items-center justify-between leading-10">
                <h3 className="flex-1 text-sm text-text-primary">{item.subject}</h3>
                <h3 className="flex-1 text-center border-l-2 text-base-primary">{item.bab}</h3>
                <h3 className="flex-1 text-center border-l-2 text-base-primary">{item.subBab}</h3>
              </div>
            )
          })
        }

        {jurusan === 'SAINTEK' &&
          matpelDataSaintek.map((item, idx) => {
            return (
              <div key={idx} className="flex items-center justify-between leading-10">
                <h3 className="flex-1 text-sm  text-text-primary">{item.subject}</h3>
                <h3 className="flex-1 text-center border-l-2 text-base-primary">{item.bab}</h3>
                <h3 className="flex-1 text-center border-l-2 text-base-primary">{item.subBab}</h3>
              </div>
            )
          })
        }

        {jurusan === 'SOSHUM' &&
          matpelDataSoshum.map((item, idx) => {
            return (
              <div key={idx} className="flex items-center text-base-primary text-opacity-70 justify-between leading-10">
                <h3 className="flex-1 text-sm text-text-primary">{item.subject}</h3>
                <h3 className="flex-1 text-center border-l-2 text-base-primary">{item.bab}</h3>
                <h3 className="flex-1 text-center border-l-2 text-base-primary">{item.subBab}</h3>
              </div>
            )
          })
        }
      </div>

      <div className="m-4 pt-4 border-t-2 mt-4">
        <h2 className="text-lg text-base-primary opacity-80 font-bold">Lama Waktu Belajarmu</h2>

        <div className="bg-white rounded-lg p-4 mt-4 pt-4">

          <h3 className="text-lg text-base-primary">Video Belajar Konsep</h3>
          <h4 className="mt-2 text-text-primary opacity-80">Saat menonton video, berapa lama waktu yang kamu butuhkan untuk memahaminya?</h4>
          <div className="flex items-center">
            <div className="bg-background w-14 h-10 py-1 px-2 rounded-lg">
              <input className="text-3xl font-bold bg-transparent" type="number" onChange={(e) => setConceptLearningTime(e.target.valueAsNumber)} value={conceptLearningTime} />
            </div>
            <h4 className="ml-2 text-text-primary opacity-80">Menit</h4>
          </div>


          <h3 className="text-lg text-base-primary mt-4 border-t-2 pt-4">Rangkuman</h3>
          <h4 className="mt-2 text-text-primary opacity-80">Saat membaca rangkuman, berapa lama waktu yang kamu butuhkan untuk memahaminya?</h4>
          <div className="flex items-center">
            <div className="bg-background w-14 h-10 py-1 px-2 rounded-lg">
              <input className="text-3xl font-bold bg-transparent" type="number" onChange={(e) => setRangkumanLearningTime(e.target.valueAsNumber)} value={rangkumanLearningTime} />
            </div>
            <h4 className="ml-2 text-text-primary opacity-80">Menit</h4>
          </div>

          <h3 className="text-lg text-base-primary mt-4 border-t-2 pt-4">Latihan Soal</h3>
          <h4 className="mt-2 text-text-primary opacity-80">Berapa banyak latihan soal yang harus kamu kerjakan untuk paham 1 subbab? </h4>
          <div className="flex items-center">
            <div className="bg-background w-14 h-10 py-1 px-2 rounded-lg">
              <input className="text-3xl font-bold bg-transparent" type="number" onChange={(e) => setJumlahSoal(e.target.valueAsNumber)} value={jumlahSoal} />
            </div>
            <h4 className="ml-2 text-text-primary opacity-80">Soal</h4>
          </div>

          <h3 className="text-lg text-base-primary mt-4 border-t-2 pt-4">Lama Pengerjaan 1 (satu) soal</h3>
          <h4 className="mt-2 text-text-primary opacity-80">Saat berlatih, berapa lama waktu kamu membaca, mengerti dan mengerjakan soal?</h4>
          <div className="flex items-center">
            <div className="bg-background w-14 h-10 py-1 px-2 rounded-lg">
              <input className="text-3xl font-bold bg-transparent" type="number" onChange={(e) => setPersoalLearningTime(e.target.valueAsNumber)} value={persoalLearningTime} />
            </div>
            <h4 className="ml-2 text-text-primary opacity-80">Menit</h4>
          </div>


          <div className="font-bold my-4 bg-yellow bg-opacity-20 py-4 px-2 rounded-lg text-center">
            <h2 className="text-text-primary">Perkiraan UTBK 2022 <span className="text-red"> 11 April 2022</span></h2>
          </div>

          <h2 className="text-base-primary font-bold">Tentukan Tanggal Mulai Belajarmu </h2>
          <div className="font-bold my-4 py-4 px-2 rounded-lg text-center bg-card-secondary">
            <input onChange={(e) => {
              const selectedDate = dayjs(e.target.value, "DD/MM/YYYY");
              setDayToUtbk(utbkDate.diff(selectedDate, 'day'));
            }} className="bg-transparent w-full" placeholder="Masukkan tanggal" type="text" onFocus={(e) => { e.target.type = 'date' }}></input>
          </div>

          <div className="flex items-center justify-between">
            <h4 className="text-text-primary text-sm">Perkiraan jumlah hari belajar</h4>
            <div className="bg-card-secondary w-24 h-10 py-1 px-2 rounded-lg text-center">
              <span className="text-3xl font-bold text-base-primary bg-transparent">{dayToUtbk}</span>
            </div>
          </div>

          <div className="mt-4 flex items-center justify-between">
            <h4 className="text-text-primary text-sm">Rata-rata Jam Belajar (per hari)</h4>
            <div className="bg-card-secondary w-24 h-10 py-1 px-2 rounded-lg text-center">
              <span className="text-3xl font-bold text-base-primary bg-transparent">{hourPerDay.toFixed(2)}</span>
            </div>
          </div>


          <div className="p-4 bg-background flex justify-between mt-6 rounded-lg">
            <div className="mr-4" ><InfoIcon />
            </div>
            <p className="text-sm font-bold leading-4 tracking-wide">
              Perhitungan waktu Hutang Belajar merupakan perkiraan, dengan asumsi tiap hari kamu belajar dengan efektif
            </p>
          </div>

        </div>

      </div>


      <div className="px-4">
        <h2 className="text-base-primary font-bold">Sudah Yakin Dengan Kemampuan Belajarmu?</h2>
        <div className="rounded-lg bg-card-secondary p-4 mt-4">
          <div className="flex justify-between text-text-primary text-xs">
            <p>Tidak Yakin</p>
            <p>Sangat Yakin</p>
          </div>
          <input onChange={
            (e) => setKeyakinan(e.target.valueAsNumber)
          } className={`w-full bg-base-primary ${styles["keyakinan-range"]}`} type="range" min={50} max={125} step={1} value={keyakinanValue} />

        </div>

        <div className="bg-card-primary mt-4 rounded-lg p-4">
          <div className="p-2 flex justify-start rounded-lg">
            <div className="mr-4" >
              <InfoIcon />
            </div>
            <div>
              <p className="text-sm text-text-primary font-bold leading-4 tracking-wide">
                Tidak Yakin:
              </p>
              <p className="mt-2 text-xs text-text-primary opacity-50 leading-4 tracking-wide">
                Sulit mengingat materi, butuh waktu lebih untuk paham, tidak bisa mengatur waktu belajar
              </p>
            </div>
          </div>

          <div className="p-2 flex justify-start rounded-lg">
            <div className="mr-4" >
              <InfoIcon />
            </div>
            <div>
              <p className="text-sm text-text-primary font-bold leading-4 tracking-wide">
                Sangat Yakin:
              </p>
              <p className="mt-2 text-xs text-text-primary opacity-50 leading-4 tracking-wide">
                Mudah mengingat materi, cepat paham, mudah semangat belajar, manajemen waktu yang baik.
              </p>
            </div>
          </div>
        </div>
      </div>

      <Footer />
    </main>
  );
}

export default Home;