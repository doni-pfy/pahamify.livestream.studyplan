import React, { useState } from "react";
import { FormattedMessage } from "react-intl";
import Slider from "react-slick";
import "slick-carousel/slick/slick-theme.css";
import "slick-carousel/slick/slick.css";
import { StudyPlanHeader } from "src/components/modules/StudyPlanHeader";
import GeneralPageHeading from "src/components/modules/GeneralPageHeading";
import { Slick } from "src/constants/slick";
import { Testimoni } from "src/constants/testimoni";
import { useUser } from "src/context/user.context";
import { StudyPlanSubjectDropdown } from "src/components/elements/StudyPlanSubjectDropdown";
import { SubmitButton } from 'src/components/elements/SubmitButton'
import { StudyPlanAccordion } from 'src/components/elements/StudyPlanAccordion';

const MAJORS = ['TPS', 'Saintek', 'Soshum']

const StudyPlan = () => {
  const [testimoni, setTestimoni] = useState(Testimoni.Content);
  const { state: { user } } = useUser();

  return (
    <main className="w-full h-full flex flex-col">
      <GeneralPageHeading className="pt-16" />
        {/* Study Plan */}
         <StudyPlanHeader className="mx-4 mb-4 mt-min-12" />
         <StudyPlanSubjectDropdown onSelected={() => {}}/>
         <div className="mx-4 mb-4">
            <Slider {...Slick.Settings}>
              {MAJORS.map((item, index) => (
                  <div key={index}>
                    <SubmitButton className="px-4 opacity-70" onClick={() => {}} textId={`major.${item}`} />
                  </div>
                ))}
            </Slider>
         </div>
         <div className="mx-2">
                <StudyPlanAccordion majorName="TPS" onSelected={() =>{}}  />
                <StudyPlanAccordion majorName="Saintek" onSelected={() =>{}}  />
                <StudyPlanAccordion majorName="Soshum" onSelected={() =>{}}  />
         </div>
         {/* End Study Plan */}
    </main>
  );
}

export default StudyPlan;