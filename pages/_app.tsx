import { ApolloProvider } from "@apollo/client"
import { AppProps } from 'next/dist/next-server/lib/router/router'
import { createApolloClient } from "src/api/apollo"
import { LocaleProvider } from "src/components/elements/LocaleProvider"
import Layout from "src/components/layouts/Layout"
import { AppStateProvider } from "src/context/appstate.context"
import { UserProvider } from "src/context/user.context"
import 'src/styles/globals.css'

function MyApp({ Component, pageProps }: AppProps) {
  const apolloClient = createApolloClient({})
  return (
    <ApolloProvider client={apolloClient}>
      <AppStateProvider>
        <LocaleProvider>
          <UserProvider >
            <Layout>
              <Component {...pageProps} />
            </Layout>
          </UserProvider>
        </LocaleProvider>
      </AppStateProvider>
    </ApolloProvider>
  );
}

export default MyApp;
