import React, { useState } from "react";
import { FormattedMessage } from "react-intl";
import Slider from "react-slick";
import "slick-carousel/slick/slick-theme.css";
import "slick-carousel/slick/slick.css";
import { StudyPlanDetailsHeader } from "src/components/modules/StudyPlanDetailsHeader";
import GeneralPageHeading from "src/components/modules/GeneralPageHeading";
import { Slick } from "src/constants/slick";
import { Testimoni } from "src/constants/testimoni";
import { useUser } from "src/context/user.context";
import { StudyPlanSubjectDropdown } from "src/components/elements/StudyPlanSubjectDropdown";
import { SubmitButton } from 'src/components/elements/SubmitButton'
import { StudyPlanDetailsAccordion } from 'src/components/elements/StudyPlanDetailsAccordion';

const MAJORS = ['TPS', 'Saintek', 'Soshum']

const StudyPlan = () => {
  const [testimoni, setTestimoni] = useState(Testimoni.Content);
  const { state: { user } } = useUser();

  return (
    <main className="w-full h-full flex flex-col">
      <GeneralPageHeading className="pt-16" />
        {/* Study Plan */}
         <StudyPlanDetailsHeader className="mx-4 mb-4 mt-min-12" />
         <div className=" mx-4 mb-4">
           
         </div>
        <StudyPlanDetailsAccordion/>
         {/* End Study Plan */}
    </main>
  );
}

export default StudyPlan;