set -e
env=""
ver=""

echo "Input Env Variable: $env"
read env
echo "Input Ver Variable: $ver"
read ver

if [ "$env" != '' ] && [ "$ver" != '' ] && ( [ "$env" == 'stg' ] || [ "$env" == 'prod' ] )
then
    echo 'valid env and ver variable'
else
    echo 'not valid env and ver. exiting...'
    exit 1
fi
echo "verified..."


if [ "$env" == 'stg' ]
then
    echo "build image and push to private registry ..."
    $("C:\Program Files\Amazon\AWSCLI\bin\aws.cmd" ecr get-login --no-include-email --region ap-southeast-1)
    docker build -t pahamify/mipi_fe_web_forum --build-arg ENV=staging --tag 402228763569.dkr.ecr.ap-southeast-1.amazonaws.com/pahamify/mipi_fe_web_forum:allv${ver} .
    docker push 402228763569.dkr.ecr.ap-southeast-1.amazonaws.com/pahamify/mipi_fe_web_forum:allv${ver}
    sed -i 's/allv/allv'${ver}'/g' deployment/stg/stg-mipi-fe-web-forum-deployment.yaml
    echo "deploying to stg..."
    kubectl apply -f deployment/stg/stg-mipi-fe-web-forum-deployment.yaml
    sed -i 's/allv'${ver}'/allv/g' deployment/stg/stg-mipi-fe-web-forum-deployment.yaml
else
    echo "build image and push to private registry ..."
    curl -X POST --data '{"text":"deployment pahamify/mipi_fe_web_forum version allv'"${ver}"' is triggered in production, please be patient the service may not responsive in a moment, sir!"}' $PHM_SLACK_HOOK_PROD
    docker build -t pahamify/mipi_fe_web_forum --build-arg ENV=production --tag asia.gcr.io/kube-prod-phm-southeast/mipi_fe_web_forum:allv${ver} .
    docker push asia.gcr.io/kube-prod-phm-southeast/mipi_fe_web_forum:allv${ver}
    sed -i 's/allv/allv'${ver}'/g' deployment/prod/prod-mipi-fe-web-forum-deployment.yaml
    echo "deploying to prod..."
    kubectl apply -f deployment/prod/prod-mipi-fe-web-forum-deployment.yaml
    sed -i 's/allv'${ver}'/allv/g' deployment/prod/prod-mipi-fe-web-forum-deployment.yaml
    curl -X POST --data '{"text":"deployment pahamify/mipi_fe_web_forum version allv'"${ver}"' has been finished, hope things goes well, sir!"}' $PHM_SLACK_HOOK_PROD
fi
echo "all task done successfully..."